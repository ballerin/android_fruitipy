package eu.fbk.mpba.app.fruitipy.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ballerin on 11/25/16.
 */

public class TestSpectrum {
    @Test
    public void spectrumIsCorrect() throws Exception {
        double[] array = {0.0,1.0,2.0,3.0};
        Spectrum mySpectrum = new Spectrum();
        mySpectrum.setSpectrum(array);
        assertEquals(array, mySpectrum.getSpectrum());
    }
    @Test (expected = IllegalArgumentException.class)
    public void spectrumIsNotEmpty() throws Exception {
        double[] array = {};
        Spectrum mySpectrum = new Spectrum();
        mySpectrum.setSpectrum(array);
    }
    @Test (expected = NullPointerException.class)
    public void spectrumIsNotNull() throws Exception {
        double[] array = null;
        Spectrum mySpectrum = new Spectrum();
        mySpectrum.setSpectrum(array);
    }
}
