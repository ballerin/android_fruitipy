package eu.fbk.mpba.app.fruitipy.network;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import eu.fbk.mpba.app.fruitipy.BuildConfig;
import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.activities.LoginActivity;
import eu.fbk.mpba.app.fruitipy.interfaces.RetrofitInterface;
import eu.fbk.mpba.app.fruitipy.models.Token;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.net.InetAddress;

import retrofit2.Callback;
import retrofit2.Response;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Retrofit;

import static eu.fbk.mpba.app.fruitipy.network.NetworkUtils.base64Credentials;
import static eu.fbk.mpba.app.fruitipy.network.NetworkUtils.getRetrofit;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by ballerin on 12/2/16.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestTokenRequest {

    private MockWebServer mockWebServer;

    @Config(sdk = 23)
    @Before
    public void populatePreferences(){
        Fruitipy.context = RuntimeEnvironment.application;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(RuntimeEnvironment.application.getApplicationContext());
        sharedPreferences.edit().putString("server_ip", "192.168.30.154").commit();
    }

    @Config(sdk = 23)
    @Test
    public void networkWorks() throws IOException {
        mockWebServer = new MockWebServer();
        Retrofit retrofit = getRetrofit("test","test").build();
        mockWebServer.enqueue(new MockResponse().setBody("{'token': '9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b'}"));
        mockWebServer.start(InetAddress.getByName(BuildConfig.SERVER_URL), BuildConfig.SERVER_PORT);
        RetrofitInterface service = retrofit.create(RetrofitInterface.class);
        Call<Token> call = service.login();
        assertTrue(call.execute() != null);
    }

    @Config(sdk = 23)
    @Test
    public void usingCorrectUrlForToken() throws Exception {
        mockWebServer = new MockWebServer();
        Retrofit retrofit = getRetrofit("test","test").build();
        mockWebServer.enqueue(new MockResponse().setBody("{'token': '9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b'}"));
        mockWebServer.start(InetAddress.getByName(BuildConfig.SERVER_URL), BuildConfig.SERVER_PORT);
        RetrofitInterface service = retrofit.create(RetrofitInterface.class);
        Call<Token> call = service.login();
        call.execute();

        if(!mockWebServer.takeRequest().getPath().equals("/"+BuildConfig.TOKEN_LOGIN_PATH)){
            fail();
        }
        //Finish web server
        mockWebServer.shutdown();
    }

    @Config(sdk = 23)
    @Test
    public void sendingCorrectDataForToken() throws Exception {
        mockWebServer = new MockWebServer();
        Retrofit retrofit = getRetrofit("test", "test").build();
        mockWebServer.enqueue(new MockResponse().setBody("{'token': '9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b'}"));
        mockWebServer.start(InetAddress.getByName(BuildConfig.SERVER_URL), BuildConfig.SERVER_PORT);
        RetrofitInterface service = retrofit.create(RetrofitInterface.class);
        String user = "test";
        String passwd = "test";
        Call<Token> call = service.login();
        call.execute();
        if (!mockWebServer.takeRequest().getHeader("Authorization").equals(base64Credentials(user,passwd))) {
            fail();
        }
    }

    @Config(sdk = 23)
    @Test
    public void errorHandlingWhenSomethingWrong() throws Exception {
        mockWebServer = new MockWebServer();
        Retrofit retrofit = getRetrofit("test", "test").build();
        mockWebServer.enqueue(new MockResponse().setBody("This is not a token"));
        mockWebServer.start(InetAddress.getByName(BuildConfig.SERVER_URL), BuildConfig.SERVER_PORT);
        RetrofitInterface service = retrofit.create(RetrofitInterface.class);
        String user = "test";
        String passwd = "test";
        service.login().enqueue(new Callback<Token>() {
               @Override
               public void onResponse(Call<Token> call, Response<Token> response) {
                   if(LoginActivity.isResponseValid(response)){
                       fail();
                   }
               }

               @Override
               public void onFailure(Call<Token> call, Throwable t) {

               }
           }

        );
    }

    @Config(sdk = 23)
    @After
    public void tearDown() throws Exception {
        mockWebServer.shutdown();
    }
}
