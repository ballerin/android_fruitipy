package eu.fbk.mpba.app.fruitipy.network;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import eu.fbk.mpba.app.fruitipy.services.SyncService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by ballerin on 1/30/17.
 */

public class TestSpectrumSync extends Activity{
    @Mock
    private SharedPreferences prefs;

    private Context context;

    @Before
    public void createPrefs(){
        prefs = Mockito.mock(SharedPreferences.class);
        this.context = Mockito.mock(Context.class);
        Mockito.when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(prefs);
    }

    @Test
    public void sendASample(){
        Intent msgIntent = new Intent(this, SyncService.class);
        //TODO: complete this test
    }
}
