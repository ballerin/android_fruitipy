package eu.fbk.mpba.app.fruitipy.DataSaved;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.widget.ShareActionProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Map;
import java.util.Set;

import static junit.framework.Assert.fail;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by ballerin on 12/5/16.
 */

public class TestTokenData {
    @Mock
    private SharedPreferences prefs;

    private Context context;

    @Before
    public void createPrefs(){
        prefs = Mockito.mock(SharedPreferences.class);
        this.context = Mockito.mock(Context.class);
        Mockito.when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(prefs);
    }

    @Test
    public void whenFirstBootTokenIsNotPresent() throws Exception{
        Mockito.when(prefs.getString(anyString(), anyString())).thenReturn(null);
        String token = prefs.getString("token", null);
        if(token != null){
            fail();
        }
    }
}