package eu.fbk.mpba.app.fruitipy.bluetooth;

import static eu.fbk.mpba.app.fruitipy.bluetooth.FakeData.generateFakeData;
import static junit.framework.Assert.fail;

import org.junit.Test;

/**
 * Created by ballerin on 12/7/16.
 */

public class TestFakeData {
    @Test
    public void testIfFakeDataIsProduced() throws Exception {
        int n = 288;
        double[] testArray = generateFakeData(n);
        if(testArray.length != n){
            fail();
        }
    }

}
