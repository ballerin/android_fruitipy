package eu.fbk.mpba.app.fruitipy.interfaces;

/**
 * Created by ballerin on 05/09/17.
 */

public interface CustomDialogInterface {

    // This is just a regular method so it can return something or
    // take arguments if you like.
    public void okButtonClicked(String value, String type);


}

