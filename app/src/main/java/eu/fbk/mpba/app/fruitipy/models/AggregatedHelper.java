package eu.fbk.mpba.app.fruitipy.models;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import eu.fbk.mpba.app.fruitipy.database.DatabaseContract;

import static eu.fbk.mpba.app.fruitipy.utils.Utils.getDb;

public class AggregatedHelper {
    public static final int SAMPLES_TO_TAKE = 20;
    static public int countAggegatedRows(AggregatedSample agToCount){
        int agCounted=0;
        SQLiteDatabase db = getDb();
        Cursor cursor = db.rawQuery("SELECT COUNT(*)"+
                " FROM " + DatabaseContract.Strings.TABLE_NAME  +
                " WHERE " + DatabaseContract.Strings.COLUMN_NAME_FRUIT + " = '" + agToCount.getFruit() + "' AND  strftime('%d-%m-%Y',datetime("+DatabaseContract.Strings.COLUMN_NAME_TIME+",'unixepoch')) = '" + agToCount.getDate() +"'", null);
        cursor.moveToFirst();
        agCounted = cursor.getInt(0);
        cursor.close();
        return agCounted;
    }
}
