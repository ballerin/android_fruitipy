package eu.fbk.mpba.app.fruitipy.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.activities.DetailActivity;
import eu.fbk.mpba.app.fruitipy.activities.ShowPhotoActivity;
import eu.fbk.mpba.app.fruitipy.models.Photo;
import eu.fbk.mpba.app.fruitipy.models.Spectrum;
import com.squareup.picasso.Picasso;

import java.util.List;

import static eu.fbk.mpba.app.fruitipy.models.PhotoHelper.getDateFromPath;
import static eu.fbk.mpba.app.fruitipy.models.PhotoHelper.getFruitFromPath;
import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.convertTimestampToReadableFormat;

/**
 * Created by ballerin on 10/2/17.
 */

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.MyViewHolder> {
    private List<Photo> samplesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView photo;
        private TextView photo_date;
        private TextView photo_fruit_type;
        private String path;


        private MyViewHolder(View view) {
            super(view);
            photo = (ImageView) view.findViewById(R.id.photo_sample);
            photo_date = (TextView) view.findViewById(R.id.date_content);
            photo_fruit_type = (TextView) view.findViewById(R.id.fruit_type_content);
        }
    }


    public PhotosAdapter(List<Photo> samplesList) {
        this.samplesList = samplesList;
    }

    @Override
    public PhotosAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photo_row, parent, false);
        return new PhotosAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PhotosAdapter.MyViewHolder holder, int position) {
        //holder.photo.setImageBitmap(ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(samplesList.get(position).getPath()), 128,128));
        Picasso.with(Fruitipy.getAppContext()).load("file://"+samplesList.get(position).getPath()).resize(250,250).centerCrop().into(holder.photo);
        holder.photo_date.setText(getDateFromPath(samplesList.get(position).getPath()));
        holder.photo_fruit_type.setText(getFruitFromPath(samplesList.get(position).getPath()));
        holder.path = samplesList.get(position).getPath();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Fruitipy.getAppContext(), ShowPhotoActivity.class);
                i.putExtra("PHOTO_PATH", holder.path);
                i.putExtra("IS_NEW", false);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return samplesList.size();
    }
}
