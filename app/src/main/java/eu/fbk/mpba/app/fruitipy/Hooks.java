package eu.fbk.mpba.app.fruitipy;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;

import eu.fbk.mpba.app.fruitipy.activities.MainActivity;
import eu.fbk.mpba.app.fruitipy.bluetooth.BLEObserver;
import eu.fbk.mpba.app.fruitipy.services.UartService;


public class Hooks {

    private final static String TAG = "BLEHOOKS";
    private final static String ACQUIRE = "R";
    private final static String INFO = "?";
    private final static int LENGTH_WHITE = 256;
    private final static int LENGTH_IR = 288;
    @SuppressLint("StaticFieldLeak")
    private static MainActivity activity;
    private static Runnable onDisconnect;
    private static Received onReceived;
    private static Progress onProgress;
    private static final boolean test = false;
    private static StringBuilder result = null;
    private static final int DIM_PACKETS = 5;
    private static int received = 0;

    public static void received(String text) {
        /*
        this function is called on every char (?) received
         */
        if (text.startsWith("[")) {
            //Begin of the line. Everything not already saved is wasted
            if (result != null)
                Log.e(TAG, "WASTED: " + result.toString());
            result = new StringBuilder();
            result.append(text);
            received = 0;
        } else if (text.endsWith("]")) {
            //End of the line
            result.append(text);
            complete(result.toString());
            received = LENGTH_IR / DIM_PACKETS;
        } else {
            //Data payload received
            result.append(text);
        }
        received++;

        //Call the Activity callback
        onProgress.acquisitionProgress(received, LENGTH_IR / DIM_PACKETS);
    }

    public static void complete(String text) {
        /*
        This function is called every time a complete row has been sent
         */
        Log.i(TAG, "received " + text);
        if (text.contains("[") && text.contains("]")) {
            String[] payload = text.substring(text.indexOf('[') + 3, text.indexOf(']')).split(" ");
            int[] payloadInChar = new int[512];
            if (payload.length < 512) {
                for (int i = 0; i < payload.length; i++) {
                    payloadInChar[i] = Integer.parseInt(payload[i].toUpperCase(), 16);
                }
                char code = payload.length == LENGTH_IR ? 'I' : payload.length == LENGTH_WHITE ? 'W' : 'X';
                if (onReceived != null) {
                    //Call the Activity callback
                    onReceived.acquisitionFinished(text, code, payloadInChar, payload.length);
                }
            } else
                Log.e(TAG, "Stringa deformata. La lunghezza non deve eguagliare o superare i 512 caratteri");
        } else
            Log.e(TAG, "Stringa non completa. '[' o ']' mancanti");
    }

    public static void connected(final MainActivity activity) {
        Hooks.activity = activity;
    }

    public static void disconnected(MainActivity mainActivity) {
        if (mainActivity == activity) {
            if (onDisconnect != null)
                onDisconnect.run();
        } else
            Log.wtf(TAG, "Le activities per la disconnessione non combaciano");
    }

    public static void acquire() {
        /*
        Inizia acquisizione
        Qeusto metodo viene chiamato dall'activity
         */
        if (activity != null) {
            if (!test)
                BLEObserver.getInstance().send(ACQUIRE);
            else
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        //TODO: understend why this data has to be sent every time
                        Intent i = new Intent(UartService.ACTION_DATA_AVAILABLE);
                        i.putExtra(UartService.EXTRA_DATA, "[bc f2 e5 46 ae 89 e7 7d 53 bc 35 fd 78 ab 5f e8 54 cf 6b 23 fd 46 ef 8e a8 e5 f6 df ca dc 73 c4 bc f2 e5 46 ae 89 e7 7d 53 bc 35 fd 78 ab 5f e8 54 cf 6b 23 fd 46 ef 8e a8 e5 f6 df ca dc 73 c4]".getBytes());
                        BLEObserver.getReceiver().onReceive(activity, i);
                    }
                }).start();
        }
        else{
            Log.e(TAG, "L'activity non deve essere null");
        }
    }

    public static void onDestroy() {
        activity = null;
    }

    public static void setOnDisconnect(Runnable callback) {
        Hooks.onDisconnect = callback;
    }

    public static void setOnReceived(Received callback) {
        Hooks.onReceived = callback;
    }

    public static void setOnProgress(Progress callback) {
        Hooks.onProgress = callback;
    }

    public interface Received {
        void acquisitionFinished(String text, char code, int[] valori, int length);
    }

    public interface Progress {
        void acquisitionProgress(int num, int outof);
    }
}