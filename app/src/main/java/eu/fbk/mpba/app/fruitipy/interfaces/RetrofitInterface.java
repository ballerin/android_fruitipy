package eu.fbk.mpba.app.fruitipy.interfaces;

import eu.fbk.mpba.app.fruitipy.BuildConfig;
import eu.fbk.mpba.app.fruitipy.models.ServerResponse;
import eu.fbk.mpba.app.fruitipy.models.Token;
import eu.fbk.mpba.app.fruitipy.network.UploadObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by ballerin on 11/28/16.
 */


public interface RetrofitInterface {
    @POST(BuildConfig.TOKEN_LOGIN_PATH)
    Call<Token> login();

    @FormUrlEncoded
    @POST(BuildConfig.SAMPLES_PATH)
    Call<ServerResponse> insert(@Field("spectrum") String spectrum,
                                @Field("fruit") String fruit,
                                @Field("gps") String gps,
                                @Field("date") String date,
                                @Field("correct") Integer correct,
                                @Field("notes") String notes,
                                @Field("uni_id") Integer uni_id);

    @Multipart
    @POST("/upload_photo/")
    Call<UploadObject> uploadFile(@Part MultipartBody.Part file, @Part("name") RequestBody name);
}

