package eu.fbk.mpba.app.fruitipy.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.Hooks;
import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.bluetooth.BLEObserver;
import eu.fbk.mpba.app.fruitipy.services.UartService;

import java.text.DateFormat;
import java.util.Date;

import static com.google.android.gms.internal.zzahg.runOnUiThread;

public class UARTReceiver extends BroadcastReceiver {

    String TAG = "UARTReceiver";
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private int mState = UART_PROFILE_DISCONNECTED;
    private int err_uart = 0;
    public UartService mService = null;
    public boolean connectedToBluetooth = false;

    public UARTReceiver(){
        super();
    }

    public void setService(UartService s){
        this.mService = s;
    }

    public UartService getService(){
        return this.mService;
    }

    public void onReceive (Context context, Intent intent) {
        String action = intent.getAction();
        if(action == null){
            action = "";
        }

        //*********************// The Device has connected
        if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
            runOnUiThread(new Runnable() {
                public void run() {
                    String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                    Log.d(TAG, "UART_CONNECT_MSG");

                    BLEObserver.getInstance().notifyConnected();
                    connectedToBluetooth = true;
                    mState = UART_PROFILE_CONNECTED;
                }
            });
        }

        //*********************// The Device has been disconnected
        if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
            runOnUiThread(new Runnable() {
                public void run() {
                    String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                    Log.d(TAG, "UART_DISCONNECT_MSG");

                    BLEObserver.getInstance().notifyDisconnected();

                    connectedToBluetooth = false;
                    mState = UART_PROFILE_DISCONNECTED;
                    if(mService!=null) {
                        mService.close();
                    }

                }
            });
        }


        //*********************//
        if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
            mService.enableTXNotification();
        }
        //*********************//
        if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {

            final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
            runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        String text = new String(txValue, "UTF-8");
                        Hooks.received(text);

                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                }
            });
        }
        //*********************// Error when uart is not supported or device is not ready
        if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)){
            if(err_uart > 1) {
                Toast.makeText(Fruitipy.getAppContext(), Fruitipy.getAppContext().getString(R.string.device_does_not_support_uart), Toast.LENGTH_LONG).show();
                mService.disconnect();
            }
            else{
                Toast.makeText(Fruitipy.getAppContext(), Fruitipy.getAppContext().getString(R.string.device_does_not_support_uart), Toast.LENGTH_LONG).show();
                err_uart++;
            }

        }

        else{
            err_uart = 0;
        }


    }
}
