package eu.fbk.mpba.app.fruitipy.network;

import eu.fbk.mpba.app.fruitipy.interfaces.RetrofitInterface;

import static eu.fbk.mpba.app.fruitipy.network.NetworkUtils.getRetrofit;

/**
 * Created by ballerin on 12/2/16.
 */

public class TokenManager {
    public static RetrofitInterface getTokenManager(String user, String passwd){

        return getRetrofit(user, passwd).build().create(RetrofitInterface.class);
    }
}
