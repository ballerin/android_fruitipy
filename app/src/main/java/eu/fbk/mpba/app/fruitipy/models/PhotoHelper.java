package eu.fbk.mpba.app.fruitipy.models;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import eu.fbk.mpba.app.fruitipy.database.DatabaseContract;

import java.util.ArrayList;
import java.util.List;

import static eu.fbk.mpba.app.fruitipy.utils.Utils.getDb;

/**
 * Created by ballerin on 10/2/17.
 * Photos are saved with this pattern:
 * FRUITIPY_ARG1_ARG2_ARG3_ARG4_ARG5
 * ARG1 = fruit (codice PICA)
 * ARG2 = GPS (LATxLON)
 * ARG3 = DATE (YYYMMDD)
 * ARG4 = TIME (HHMMSS)
 * ARG5 = TODO: I don't remember what this was used for
 */

public class PhotoHelper {

    static public String getFruitFromPath(String path){
        String[] parts = path.split("_");
        String result="";
        if (parts[1].equals("")) {
            result = "UNKNOWN";
        }
        else{
            result = parts[1];
        }
        return result;
    }

    static public String getDateFromPath(String path){
        String[] parts = path.split("_");
        return parts[3].substring(6,8) + "-" + parts[3].substring(4,6) + "-" + parts[3].substring(0,4);
    }

    static public String getReversedDateFromPath(String path){
        /*
        From standard dd-mm-yyyy to retarded date format yyyy-mm-dd
         */
        String[] parts = path.split("_");
        return parts[3].substring(0,4) + "-" + parts[3].substring(4,6) + "-" + parts[3].substring(6,8);
    }
    static public String getReversedDateFromDate(String date){
        /*
        From standard dd-mm-yyyy to retarded date format yyyy-mm-dd
         */
        return date.substring(6,10) + "-" + date.substring(3,5) + "-" + date.substring(0,2);
    }

    static public String getLocationFromPath(String path){
        String[] parts = path.split("_");
        return parts[2].replace("x",",");
    }

    public static String getTimeFromPath(String path){
        String[] parts = path.split("_");
        return parts[4].substring(0,2) + "-" + parts[4].substring(2,4) + "-" +parts[4].substring(4,6);
    }

    /**
     * In this case we do not really care about the order, since this function is used to generate aggregated data rows
     */
    public static List<Photo> retrieveAggregatedPhotos(){

        List photos = new ArrayList<Photo>();

        SQLiteDatabase db = getDb();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) as count, " + DatabaseContract.Strings.COLUMN_NAME_FRUIT + "," + DatabaseContract.Strings.COLUMN_NAME_DATE + " as day, " + DatabaseContract.Strings.COLUMN_NAME_PATH + " " +
                " FROM " + DatabaseContract.Strings.PHOTOS_TABLE_NAME +
                " GROUP BY day, "+DatabaseContract.Strings.COLUMN_NAME_FRUIT, null);
        int count = cursor.getCount();
        // Defined Array values to show in ListView
        do{
            cursor.moveToNext();
            if(!cursor.isAfterLast()) {
                String path = (cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_PATH)));
                String fruit = (cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_FRUIT)));
                String date = PhotoHelper.getDateFromPath(path);
                photos.add(new Photo(path, fruit, date));
            }
        }while(!cursor.isLast() && !cursor.isAfterLast());
        cursor.close();

        return photos;
    }

    /**
     * We have to reverse the date to yyy-mm-
     *
     * The result is ordered by:
     * - date (DESC)
     * - time (DESC)
     * so that the last photo taken is the first in the list
     */
    public static List<Photo> retrievePhotosByFruitAndDate(String fruit, String date){

        date = getReversedDateFromDate(date);

        List photos = new ArrayList<Photo>();

        SQLiteDatabase db = getDb();
        Cursor cursor = db.rawQuery("SELECT " +DatabaseContract.Strings.COLUMN_NAME_PATH+ " " +
                " FROM " + DatabaseContract.Strings.PHOTOS_TABLE_NAME +
                " WHERE " + DatabaseContract.Strings.COLUMN_NAME_FRUIT + " = '" + fruit + "' AND  " + DatabaseContract.Strings.COLUMN_NAME_DATE + " = '" + date +"'" +
                " ORDER BY " + DatabaseContract.Strings.COLUMN_NAME_DATE + " DESC, " + DatabaseContract.Strings.COLUMN_NAME_TIME + " DESC", null);
        int count = cursor.getCount();
        // Defined Array values to show in ListView
        do{
            cursor.moveToNext();
            if(!cursor.isAfterLast()) {
                String path = (cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_PATH)));
                photos.add(new Photo(path, fruit, date));
            }
        }while(!cursor.isLast() && !cursor.isAfterLast());
        cursor.close();
        return photos;
    }

}
