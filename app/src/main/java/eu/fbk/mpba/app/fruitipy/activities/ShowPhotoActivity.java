package eu.fbk.mpba.app.fruitipy.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.models.PhotoHelper;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ballerin on 10/2/17.
 */

public class ShowPhotoActivity extends AppCompatActivity {
    private static final String TAG = "SHOWPHOTO";

    private ImageView photoView;
    private TextView photo_date;
    private TextView photo_fruit;
    private TextView photo_gps;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        ButterKnife.bind(this);

        photoView = (ImageView) findViewById(R.id.big_photo_detail);
        photo_date = (TextView) findViewById(R.id.date_content);
        photo_fruit = (TextView) findViewById(R.id.fruit_type_content);
        photo_gps = (TextView) findViewById(R.id.edit_gps);
        path = getIntent().getExtras().getString("PHOTO_PATH");
        Bitmap myBitmap = BitmapFactory.decodeFile(path);
        try {
            myBitmap = modifyOrientation(myBitmap, path);
        }
        catch(IOException ex){
            Log.e(TAG, "Error rotating image");
        }
        photoView.setImageBitmap(myBitmap);
        photo_date.setText(PhotoHelper.getDateFromPath(path));
        photo_fruit.setText(PhotoHelper.getFruitFromPath(path));
        photo_gps.setText(PhotoHelper.getLocationFromPath(path));
        boolean isNew = true;

        if(getIntent().hasExtra("IS_NEW")) {
            isNew = getIntent().getExtras().getBoolean("IS_NEW");
        }
        if(isNew) {
            (findViewById(R.id.another_sample_button)).setVisibility(View.VISIBLE);
        }

        setResult(RESULT_CANCELED);
    }

    @OnClick(R.id.another_sample_button)
    public void takeAnotherSample(){
        Intent data = new Intent();
        data.setData(Uri.parse("photo"));
        setResult(RESULT_OK, data);
        finish();
    }
    @OnClick(R.id.gps_linear_layout)
    public void showGPSOnMap() {
        Uri gmmIntentUri = Uri.parse("geo:" + PhotoHelper.getLocationFromPath(path) + "?q=" + PhotoHelper.getLocationFromPath(path) + getString(R.string.registration_point));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        startActivity(mapIntent);
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

}
