package eu.fbk.mpba.app.fruitipy.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Switch;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.interfaces.CustomDialogInterface;
import eu.fbk.mpba.app.fruitipy.R;

/**
 * Created by ballerin on 2/20/17.
 */

public class SelectFruit extends DialogFragment{

    CustomDialogInterface customDI;
    EditText editFruit;
    Switch takePhoto;

    public SelectFruit newInstance() {
        SelectFruit frag = new SelectFruit();
        return frag;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle mArgs = getArguments();

        String last_fruit = mArgs.getString("last_fruit");
        editFruit = new EditText(Fruitipy.getAppContext());
        editFruit.setText(last_fruit);
        editFruit.setTextColor(Color.parseColor("#000000"));
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.insert_fruit) //Don't know why but this does not work on instant run
                .setView(editFruit)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                customDI.okButtonClicked(editFruit.getText().toString(), mArgs.getString("sample_type"));
                                dialog.dismiss();
                            }
                });
        setStyle(DialogFragment.STYLE_NORMAL, R.style.SelectFruitFragmentStyle);
        return builder.create();
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        customDI = (CustomDialogInterface) a;
    }

}
