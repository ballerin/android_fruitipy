package eu.fbk.mpba.app.fruitipy.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.activities.DetailActivity;
import eu.fbk.mpba.app.fruitipy.activities.SamplesListActivity;
import eu.fbk.mpba.app.fruitipy.models.AggregatedHelper;
import eu.fbk.mpba.app.fruitipy.models.AggregatedSample;
import eu.fbk.mpba.app.fruitipy.models.Spectrum;

import java.util.List;

import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.convertTimestampToReadableFormat;

/**
 * Created by ballerin on 24/07/2018.
 */

public class AggregatedSamplesAdapter extends RecyclerView.Adapter<AggregatedSamplesAdapter.MyViewHolder> {

    private List<AggregatedSample> samplesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView time;
        public TextView id;
        public TextView fruit;
        public TextView aggregatedCounter;

        public MyViewHolder(View view) {
            super(view);
            time = (TextView) view.findViewById(R.id.itemset_time);
            id = (TextView) view.findViewById(R.id.itemset_id);
            fruit = (TextView) view.findViewById(R.id.itemset_fruit);
            aggregatedCounter = (TextView) view.findViewById(R.id.aggregated_counter);
        }
    }


    public AggregatedSamplesAdapter(List<AggregatedSample> samplesList) {
        this.samplesList = samplesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_aggregated_sample, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AggregatedSample sample = samplesList.get(position);
        holder.time.setText(sample.getDate());
        holder.fruit.setText(sample.getFruit());
        holder.aggregatedCounter.setText(sample.getCount()+"/"+AggregatedHelper.SAMPLES_TO_TAKE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Fruitipy.getAppContext(), SamplesListActivity.class);
                i.putExtra("DATE", sample.getDate());
                i.putExtra("FRUIT", sample.getFruit());
                i.putExtra("IS_NEW", false);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return samplesList.size();
    }
}