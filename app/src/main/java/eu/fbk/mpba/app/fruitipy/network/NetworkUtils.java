package eu.fbk.mpba.app.fruitipy.network;

import android.preference.PreferenceManager;
import android.util.Base64;

import eu.fbk.mpba.app.fruitipy.BuildConfig;
import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.interfaces.RetrofitInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ballerin on 2/12/16.
 *
 * Network utils to communicate with the django server
 */

public class NetworkUtils {

    //The gson is only one because it is the same for every method in the package
    private static Gson gson = null;

    /**
     * This function initializes the gson variable
     */
    private static void buildGsonIfNull(){
        if (gson == null){
            gson = new GsonBuilder()
                    .setLenient()
                    .create();
        }
    }

    /**
     * Turns the django user and password into the base64 format accepted by django
     * @param user django user
     * @param passwd django password
     * @return the base64 encoded credentials
     */
    public static String base64Credentials(String user, String passwd){
        String credentials = user + ":" + passwd;
        return "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
    }

    /**
     * Returns a Retrofit builder for the token authentication
     *
     * If the user or the password is null then an exception will be thrown
     *
     * @param user django user
     * @param passwd django password
     * @return Retrofit Builder
     */
    public static Retrofit.Builder getRetrofit(String user, String passwd) {

        buildGsonIfNull();

        Retrofit.Builder retrofit = null;
        if(user != null && passwd != null) {
            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.addInterceptor(chain -> {
                Request request = chain.request().newBuilder().addHeader("Authorization", base64Credentials(user, passwd)).addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            });

            OkHttpClient client = builder.build();

            return  new Retrofit.Builder()
                    .baseUrl("http://" + PreferenceManager.getDefaultSharedPreferences(Fruitipy.getAppContext()).getString("server_ip", BuildConfig.SERVER_URL) + ":" + BuildConfig.SERVER_PORT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson));
        }
        else{
            throw new IllegalArgumentException();
        }
    }


    /**
     * Builder for the retrofit request
     *
     * @param token django auth token
     * @return RetrofitInterface
     */
    public static RetrofitInterface getSpectrumRetrofit(String token){

        buildGsonIfNull();

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Token " + token).addHeader("Accept", "application/json").build();
            return chain.proceed(request);
        });

        OkHttpClient client = builder.build();
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .baseUrl("http://" + PreferenceManager.getDefaultSharedPreferences(Fruitipy.getAppContext()).getString("server_ip", BuildConfig.SERVER_URL) + ":" + BuildConfig.SERVER_PORT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson));

        return retrofit.build().create(RetrofitInterface.class);
    }

    /**
     * Builder for the retrofit request
     *
     * @param token django auth token
     * @return RetrofitInterface
     */
    public static Retrofit getPhotoRetrofit(String token){

        buildGsonIfNull();

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();


        builder.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Token " + token).addHeader("Accept", "application/json").build();
            return chain.proceed(request);
        });

        OkHttpClient client = builder.readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(5, TimeUnit.MINUTES).build();

        return  new Retrofit.Builder()
                .baseUrl("http://"+ PreferenceManager.getDefaultSharedPreferences(Fruitipy.getAppContext()).getString("server_ip", BuildConfig.SERVER_URL) + ":" + BuildConfig.SERVER_PORT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * TODO: method to check internet connection (to avoid unnecessary requests)
     * @return true if an internet connection is established, false otherwise
     */
    private boolean isDeviceConnectedToServer(){
        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL(
                    BuildConfig.SERVER_URL).openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(3000);
            urlc.setReadTimeout(4000);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            return (false);
        }
    }
}
