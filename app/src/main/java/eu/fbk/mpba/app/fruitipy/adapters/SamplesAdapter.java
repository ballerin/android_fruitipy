package eu.fbk.mpba.app.fruitipy.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import eu.fbk.mpba.app.fruitipy.activities.DetailActivity;
import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.models.Spectrum;
import eu.fbk.mpba.app.fruitipy.R;

/**
 * Created by ballerin on 2/13/17.
 */

import android.view.View;

import java.util.List;

import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.convertTimestampToReadableFormat;

public class SamplesAdapter extends RecyclerView.Adapter<SamplesAdapter.MyViewHolder> {

    private List<Spectrum> samplesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView time;
        public TextView id;
        public TextView fruit;

        public MyViewHolder(View view) {
            super(view);
            time = (TextView) view.findViewById(R.id.itemset_time);
            id = (TextView) view.findViewById(R.id.itemset_id);
            fruit = (TextView) view.findViewById(R.id.itemset_fruit);
        }
    }


    public SamplesAdapter(List<Spectrum> samplesList) {
        this.samplesList = samplesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sample_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Spectrum sample = samplesList.get(position);
        holder.time.setText(convertTimestampToReadableFormat(sample.getTime()));
        holder.id.setText(String.valueOf(sample.getId()));
        holder.fruit.setText(sample.getFruit());
        long id = sample.getId();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Fruitipy.getAppContext(), DetailActivity.class);
                i.putExtra("SAMPLE_ID", id);
                i.putExtra("IS_NEW", false);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return samplesList.size();
    }
}