package eu.fbk.mpba.app.fruitipy.bluetooth;

public interface UARTCallbacks {
    public void onDeviceConnected();
    public void onDeviceDisconnected();
    public void onConnectionError();
}
