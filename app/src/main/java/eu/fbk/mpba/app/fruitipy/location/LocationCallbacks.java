package eu.fbk.mpba.app.fruitipy.location;

import android.location.Location;

public interface LocationCallbacks {
    public void onLocationUpdated(Location l, boolean m, boolean e);
}