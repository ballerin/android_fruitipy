package eu.fbk.mpba.app.fruitipy.models;

/**
 * Created by ballerin on 10/2/17.
 */

public class Photo {
    private int id;
    private String path;
    private String date;

    private String fruit;

    private Photo(){}

    public Photo(String path, String fruit, String date){
        this.path = path;
        this.fruit = fruit;
        this.date = date;
    }

    public void setPath(String mPath){
        this.path = mPath;
    }

    public String getPath(){
        return this.path;
    }

    public String getFruit() {
        return fruit;
    }

    public void setFruit(String fruit) {
        this.fruit = fruit;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
