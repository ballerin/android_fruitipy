package eu.fbk.mpba.app.fruitipy.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.getSpectrumById;

public class AggregatedSample implements Comparable<AggregatedSample> {
    private String date;
    private String fruit;
    private int count;
    private long exampleTime;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFruit() {
        return fruit;
    }

    public void setFruit(String fruit) {
        this.fruit = fruit;
    }

    public long getExampleTime() {
        return exampleTime;
    }

    public void setExampleTime(long exampleTime) {
        this.exampleTime = exampleTime;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void generateFromId(long id){
        Spectrum s = getSpectrumById(id);
        this.setFruit(s.getFruit());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ITALY);
        sdf.setTimeZone(Calendar.getInstance().getTimeZone());
        String formatted_time = sdf.format(s.getTime()* 1000);
        this.setDate(formatted_time);
        this.setExampleTime(s.getTime());
    }

    public int compareTo(AggregatedSample ag){
        return -(int)(this.exampleTime-ag.exampleTime);
    }

    @Override
    public boolean equals(Object o){
        if(o==null){
            return false;
        }
        else if(! (o instanceof AggregatedSample)){
            return false;
        }
        else if(o == this){
            return true;
        }
        else {
            return ((AggregatedSample)o).getFruit().equals(this.getFruit()) && ((AggregatedSample)o).getDate().equals(this.getDate());
        }
    }

    @Override
    public int hashCode(){
        //TODO: make it more efficient
        return 0;
    }
}
