package eu.fbk.mpba.app.fruitipy.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.receivers.UARTReceiver;
import eu.fbk.mpba.app.fruitipy.services.UartService;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;

public class BLEObserver {

    private static final String TAG = "BLEOBS";

    private static BLEObserver instance = null;
    private static UARTReceiver receiver = null;
    private static BluetoothDevice mDevice = null;

    private static HashSet<UARTCallbacks> activities = new HashSet<>();

    private BLEObserver() {}

    private static void init(){
        instance = new BLEObserver();
        receiver = new UARTReceiver();
        instance.service_init();
    }

    public static BLEObserver getInstance() {
        if (instance == null) {
            init();

        }
        return instance;
    }

    public static UARTReceiver getReceiver() {
        if (instance == null) {
            init();
        }
        return receiver;
    }

    public void subscribe(UARTCallbacks a) {
        activities.add(a);
    }

    public void unsubscribe(UARTCallbacks a) {
        activities.remove(a);
    }

    public void notifyConnected(){
        for(UARTCallbacks a:activities){
            a.onDeviceConnected();
        }
    }

    public void notifyDisconnected(){
        for(UARTCallbacks a:activities){
            a.onDeviceDisconnected();
        }
    }

    public void notifyError(){
        for(UARTCallbacks a:activities){
            a.onConnectionError();
        }
    }

    public void connectTo(String deviceAddress){
        Log.i(TAG, "Connecting to device");
        mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);
        boolean c = getReceiver().mService.connect(deviceAddress);
        Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + getReceiver().mService);
        if(c) {
            showMessage(Fruitipy.getAppContext().getString(R.string.toast_device_connected_to, mDevice));
        }
        else{
            showMessage("Connection error");
        }
    }

    public void disconnect(){
        //showMessage("Disconnected");
        if (mDevice!=null)
        {
            getReceiver().mService.disconnect();
        }
        getReceiver().connectedToBluetooth = false;
        notifyDisconnected();
    }

    public boolean isConnected(){
        return getReceiver().connectedToBluetooth;
    }

    //UART service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            getReceiver().mService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + getReceiver().mService);
            if (!getReceiver().mService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
            }

        }

        public void onServiceDisconnected(ComponentName classname) {
            ////     mService.disconnect(mDevice);
            getReceiver().mService = null;
        }
    };

    private void service_init() {
        Intent bindIntent = new Intent(Fruitipy.getAppContext(), UartService.class);
        Fruitipy.getAppContext().bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(Fruitipy.getAppContext()).registerReceiver(getReceiver(), makeGattUpdateIntentFilter());
    }

    private IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }

    private void showMessage(String msg){
        Toast.makeText(Fruitipy.getAppContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void send(String message) {
        byte[] value;
        try {
            //send data to service
            value = message.getBytes("UTF-8");
            getReceiver().mService.writeRXCharacteristic(value);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
        }
    }

}
