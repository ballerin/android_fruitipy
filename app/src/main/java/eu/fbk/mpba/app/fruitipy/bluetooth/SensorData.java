package eu.fbk.mpba.app.fruitipy.bluetooth;

import static eu.fbk.mpba.app.fruitipy.bluetooth.FakeData.generateFakeData;

/**
 * Created by ballerin on 12/7/16.
 */

public class SensorData {
    double[] data;
    int n = 288;
    public SensorData getDataFromSensor() {
        data = generateFakeData(n);
        return this;
    }

    public String spectrumToString(){
        String sSpectrum = "";
        for(int i=0; i<n-1; i++){
            sSpectrum += String.valueOf(data[i]).substring(0,4) + ",";
        }
        sSpectrum += String.valueOf(data[n-1]).substring(0,4);
        return sSpectrum;
    }
}
