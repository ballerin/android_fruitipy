package eu.fbk.mpba.app.fruitipy.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class ServerResponse {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("uni_id")
    @Expose
    private Integer uniId;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getUniId() {
        return uniId;
    }

    public void setUniId(Integer uniId) {
        this.uniId = uniId;
    }

}