package eu.fbk.mpba.app.fruitipy;

import android.app.Application;
import android.content.Context;

/**
 * Created by ballerin on 2/7/17.
 */

public class Fruitipy extends Application {
    public static Context context;

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getAppContext() {
        return context;
    }
}
