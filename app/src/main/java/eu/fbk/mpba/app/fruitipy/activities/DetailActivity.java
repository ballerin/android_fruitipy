package eu.fbk.mpba.app.fruitipy.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import eu.fbk.mpba.app.fruitipy.models.AggregatedHelper;
import eu.fbk.mpba.app.fruitipy.models.AggregatedSample;
import eu.fbk.mpba.app.fruitipy.models.Spectrum;
import eu.fbk.mpba.app.fruitipy.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.getSpectrumById;

/**
 * Created by ballerin on 1/30/17.
 */

public class DetailActivity extends AppCompatActivity {

    static Spectrum sample;
    static TextView ResultDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        ButterKnife.bind(this);

        ResultDetail = (TextView) findViewById(R.id.result_detail);

        sample = getSpectrumById(getIntent().getExtras().getLong("SAMPLE_ID"));

        if(sample.getResult() != null && !sample.getResult().equals("")){
            ResultDetail.setText(sample.getResult());
        }

        boolean isNew = true;

        if(getIntent().hasExtra("IS_NEW")) {
            isNew = getIntent().getExtras().getBoolean("IS_NEW");
        }


        if(isNew) {
            (findViewById(R.id.another_sample_button)).setVisibility(View.VISIBLE);
        }

        TextView fruit_content = (TextView) findViewById(R.id.fruit_content);
        fruit_content.setText(sample.getFruit());

        TextView aggregatedCounter = (TextView) findViewById(R.id.aggregated_counter);
        AggregatedSample ag = new AggregatedSample(); ag.generateFromId(getIntent().getExtras().getLong("SAMPLE_ID"));
        aggregatedCounter.setText(AggregatedHelper.countAggegatedRows(ag)+"/"+AggregatedHelper.SAMPLES_TO_TAKE);

        drawSpectrumGraph();

        setResult(RESULT_CANCELED);
    }

    private void drawSpectrumGraph(){
        DataPoint[] points = new DataPoint[sample.getSpectrum().length];
        double maxy = 252;
        for(int i=0; i<sample.getSpectrum().length; i++){
            points[i] = new DataPoint(i,sample.getSpectrum()[i]);
            if (sample.getSpectrum()[i] > maxy)
                maxy = sample.getSpectrum()[i];
        }

        GraphView graph = (GraphView) findViewById(R.id.graph);

        //Set manually the bounds because auto range is not so accurate
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(sample.getSpectrum().length-1 );
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY((int)maxy + 10);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setXAxisBoundsManual(true);

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(points);
        series.setThickness(2);
        graph.addSeries(series);
    }

    @OnClick(R.id.another_sample_button)
    public void takeAnotherSample(){
        Intent data = new Intent();
        data.setData(Uri.parse("sample"));
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            case R.id.edit_button:
                Intent i = new Intent(this, EditActivity.class);
                i.putExtra("SAMPLE_ID", getIntent().getExtras().getLong("SAMPLE_ID"));
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    public static void newDataSynced(long id){
        if(sample != null) {
            if (sample.getId() == id) {
                sample = getSpectrumById(id);
                ResultDetail.setText(sample.getResult());
            }
        }
    }

}
