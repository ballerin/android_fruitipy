package eu.fbk.mpba.app.fruitipy.bluetooth;

/**
 * Created by ballerin on 12/7/16.
 */

public class FakeData {
    public static double[] generateFakeData(int n){
        /*
         * Given a number n returns n random float
         */
        double[] Data = new double[n];
        for(int i=0; i<n; i++){
            Data[i] = Math.random();
        }
        return Data;
    }
}
