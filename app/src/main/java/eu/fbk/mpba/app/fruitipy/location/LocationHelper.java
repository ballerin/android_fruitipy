package eu.fbk.mpba.app.fruitipy.location;

import android.content.Context;
import android.location.Location;
import android.os.SystemClock;
import android.util.Log;
import android.widget.TextView;

import eu.fbk.mpba.app.fruitipy.R;

public class LocationHelper {

    /**
     * Method called by the onLocationUpdated callback from LocationCallbacks
     * This is here just to condensate the code since the same function is used in more than
     * one activity
     */
    static public void onLocationUpdated(Location l, boolean m, boolean e, Context context, TextView bottomBanner){

        if(l == null || e){
            //Error or no location registered
            bottomBanner.setBackgroundColor(context.getResources().getColor(R.color.gps_bad));
            bottomBanner.setText("ERROR");
        }
        else {
            //Location available
            String latitude = String.valueOf(l.getLatitude());
            String longitude = String.valueOf(l.getLongitude());
            String accuracy = String.valueOf(l.getAccuracy());
            Long nanos = SystemClock.elapsedRealtimeNanos() - l.getElapsedRealtimeNanos();
            Long minutes = nanos/ 600000000 / 10;
            if (m) {
                bottomBanner.setBackgroundColor(context.getResources().getColor(R.color.gps_medium));
                bottomBanner.setText(context.getString(R.string.location_manually_set, latitude.substring(0, 6), longitude.substring(0, 6)));
            } else {
                bottomBanner.setText(context.getString(R.string.location_accuracy, String.valueOf(accuracy)));
                if (minutes > 10 || Double.parseDouble(accuracy)>800) {
                    bottomBanner.setBackgroundColor(context.getResources().getColor(R.color.gps_bad));
                } else if (minutes > 5 || Double.parseDouble(accuracy)>100) {
                    bottomBanner.setBackgroundColor(context.getResources().getColor(R.color.gps_medium));
                } else {
                    bottomBanner.setBackgroundColor(context.getResources().getColor(R.color.gps_good));
                }
            }
            //Toast.makeText(context, ""+minutes, Toast.LENGTH_SHORT).show();
            Log.i("LOCATION", minutes.toString());
        }
    }
}
