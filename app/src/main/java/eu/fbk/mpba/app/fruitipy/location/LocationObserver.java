package eu.fbk.mpba.app.fruitipy.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.utils.Utils;

import java.util.HashSet;


/**
 * This Observer is used to notify every Activity requesting Location Updates of new Updates.
 * Every minute the last update is sent no matter what (to prevent no notification on the
 * activity level if no real Location Update is sent)
 */
public class LocationObserver {

    private static final String TAG = "LOCOBS";
    //Location updates when the activity is visible
    private static final String GPSlocationProvider = LocationManager.GPS_PROVIDER;
    private static final String NETWORKlocationProvider = LocationManager.NETWORK_PROVIDER;
    private static LocationObserver instance = null;

    private LocationManager locationManager;
    private static LocationListener locationListener;
    private static Location location;
    private static boolean lastManual = false;
    private static boolean lastError = false;
    private static Runnable reminder;
    private static Handler mHandler;
    private static HashSet<LocationCallbacks> activities = new HashSet<>();

    private LocationObserver() {
        locationManager =  (LocationManager) Fruitipy.getAppContext().getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            public void onLocationChanged(Location newLocation) {
                // Called when a new location is found by the network location provider.
                if(Utils.isBetterLocation(newLocation, location)){
                    location = newLocation;
                }
                //Toast.makeText(Fruitipy.getAppContext(), "Accuracy: " + String.valueOf(location.getAccuracy()) + " from " + String.valueOf(location.getProvider()), Toast.LENGTH_SHORT).show();
                notifyLocationUpdate(false);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        mHandler = new Handler();
    }

    /**
     * When the app is closed and the Garbage Collector runs unsubscribe from locationManager
     */
    @Override
    public void finalize(){

        try {
            locationManager.removeUpdates(locationListener);
        }
        catch(SecurityException e){
            Toast.makeText(Fruitipy.getAppContext(), "Security exception with locationUpdates", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Instance the scheduler for the 'every-minute-update' process
     */
    public static LocationObserver getInstance(){
        if (instance == null){
            instance = new LocationObserver();
            reminder = new Runnable(){
                @Override
                public void run() {
                    LocationObserver.getInstance().notify(lastManual,lastError);
                    mHandler.postDelayed(reminder, 1000 * 60); //Every minute
                }
            };
            reminder.run();
        }
        return instance;
    }

    /**
     * Subscribe to the Observer.
     * Now is the right time to check if the settings for the manual location has changed
     */
    public void subscribe(LocationCallbacks a) {
        activities.add(a);
        if(!PreferenceManager.getDefaultSharedPreferences(Fruitipy.getAppContext()).getBoolean("preference_use_fake_position", false)) {
            //Use real position
            try {
                locationManager.requestLocationUpdates(GPSlocationProvider, 0, 0, locationListener);
                locationManager.requestLocationUpdates(NETWORKlocationProvider, 0, 0, locationListener);
                Location tmp_location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if(tmp_location != null){
                    location = tmp_location;
                }
                if(location != null) {
                    Long nanos = SystemClock.elapsedRealtimeNanos() - location.getElapsedRealtimeNanos();
                    notifyLocationUpdate(false);
                    //BottomBanner.setText(getString(R.string.last_position, String.valueOf(nanos / 600000000 / 100)));
                }
            } catch (SecurityException e) {
                Toast.makeText(Fruitipy.getAppContext(), "Security exception with locationUpdates", Toast.LENGTH_LONG).show();
            }
        }
        else{
            //Use fake position

            if(locationManager != null){
                locationManager.removeUpdates(locationListener);
            }
            try {
                String position = PreferenceManager.getDefaultSharedPreferences(Fruitipy.getAppContext()).getString("preference_set_fake_position", ",");
                String latitude = position.split(",")[0] + "      ";
                String longitude = position.split(",")[1] + "      ";
                location = new Location("");
                location.setLatitude(Float.valueOf(latitude));
                location.setLongitude(Float.valueOf(longitude));
                location.setAccuracy(0);
                notifyLocationUpdate(true);
            }
            catch (Exception e){
                Toast.makeText(Fruitipy.getAppContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                notifyError();
            }
        }
        notifyLocationUpdate(lastManual);
    }

    public void unsubscribe(LocationCallbacks a) {
        activities.remove(a);
    }

    private void notifyError(){
        notify(true, true);
    }

    private void notifyLocationUpdate(boolean manually){
        notify(manually, false);
    }

    private void notify(boolean manually, boolean error){
        lastManual = manually;
        lastError = error;
        for(LocationCallbacks a:activities){
            a.onLocationUpdated(location, manually, error);
        }
    }
}
