package eu.fbk.mpba.app.fruitipy.models;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import eu.fbk.mpba.app.fruitipy.database.DatabaseContract;
import eu.fbk.mpba.app.fruitipy.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by ballerin on 2/7/17.
 */

public class SpectrumHelper {
    static public Spectrum getSpectrumById(long id) {
        Spectrum s = new Spectrum();
        SQLiteDatabase db = Utils.getDb();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DatabaseContract.Strings._ID,
                DatabaseContract.Strings.COLUMN_NAME_SPECTRUM,
                DatabaseContract.Strings.COLUMN_NAME_FRUIT,
                DatabaseContract.Strings.COLUMN_NAME_GPS,
                DatabaseContract.Strings.COLUMN_NAME_RESULT,
                DatabaseContract.Strings.COLUMN_NAME_IS_CORRECT,
                DatabaseContract.Strings.COLUMN_NAME_NOTES,
                DatabaseContract.Strings.COLUMN_NAME_TIME,
                DatabaseContract.Strings.COLUMN_NAME_UNIVERSAL_ID,
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = DatabaseContract.Strings._ID + " = ?";
        String[] selectionArgs = {Long.valueOf(id).toString()};

        Cursor cursor = db.query(
                DatabaseContract.Strings.TABLE_NAME,      // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // The sort order
        );

        cursor.moveToNext(); //Just one time, it is only a value

        //Getting spectrum
        String[] spectrum_string = cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_SPECTRUM)).split(",");
        double[] temp = new double[spectrum_string.length];
        for (int i = 0; i < spectrum_string.length; i++) {
            temp[i] = Double.valueOf(spectrum_string[i]);
        }
        s.setSpectrum(temp);

        //Getting fruit type
        s.setFruit(cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_FRUIT)));

        //Getting gps data
        String gps_string = cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_GPS));

        if(gps_string != null) {
            if (!gps_string.equals("n/d") && !gps_string.equals("")) {
                s.setGps(Double.valueOf(gps_string.split("/")[0]), Double.valueOf(gps_string.split("/")[1]));
            }
        }
        else{
            s.setGps(0.00,0.00);
        }

        //Getting timestamp
        s.setTime(Long.valueOf(cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_TIME))));

        //Getting result
        s.setResult(cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_RESULT)));

        //Getting isCorrect
        s.setCorrect(Integer.valueOf(cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_IS_CORRECT))));

        //Getting notes
        s.setNotes(cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_NOTES)));

        //Getting universal_id
        s.setUni(Integer.valueOf(cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_UNIVERSAL_ID))));

        cursor.close();

        s.setId(id);

        return s;
    }

    static public String convertTimestampToReadableFormat(long time){
        /*
        Because humans don't like to see unix timestamps
         */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd','HH:mm:ss");
        sdf.setTimeZone(Calendar.getInstance().getTimeZone());
        String formatted_time = sdf.format(time* 1000);
        return formatted_time;
    }
}
