package eu.fbk.mpba.app.fruitipy.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;

import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.services.SyncService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/*
This should become a transition activity where all permissions are requested
 */

public class StartActivity extends AppCompatActivity {

    private String requiredPermissions[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        askPermissions();
    }

    protected String[] getRequiredPermissions() {
        if (requiredPermissions == null) {
            try {
                // try to get app permissions from manifest declarations
                PackageManager pm = getApplicationContext().getPackageManager();
                PackageInfo info = pm.getPackageInfo(getPackageName(), PackageManager.GET_PERMISSIONS);
                requiredPermissions = info.requestedPermissions;
            }
            catch (Exception e) {
                Log.wtf("Package info", "Couldn't get this app's package info");
            }
        }

        return requiredPermissions;
    }

    public String[] getUnacceptedPermissions() {
        List<String> unacceptedPermissions = new ArrayList<>();

        for (String permission: getRequiredPermissions()) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                unacceptedPermissions.add(permission);
            }
        }

        return unacceptedPermissions.toArray(new String[]{});
    }

    public void askPermissions() {
        String permissions[] = getUnacceptedPermissions();

        if (permissions.length > 0) {
            ActivityCompat.requestPermissions(this, permissions, 1337);
        } else {
            allPermissionsAccepted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        boolean permissionsRefused = false, neverAskAgain = false;

        for (int i=0, j=permissions.length; i<j; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                permissionsRefused = true;
                neverAskAgain = neverAskAgain || !ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]);
            }
        }

        if (neverAskAgain) {
            // the user has permanently declined a permission
            new AlertDialog.Builder(this)
                    .setTitle(R.string.permissions_required_title)
                    .setMessage(R.string.permissions_required)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {

                            Intent i = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            i.setData(Uri.fromParts("package", getPackageName(), null));
                            startActivity(i);

                        }
                    })
                    .setNegativeButton(R.string.exit,  new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            System.exit(0);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        else if (permissionsRefused) {
            // not all permissions have been accepted
            askPermissions();
        }

        else {
            allPermissionsAccepted();
        }
    }

    private void allPermissionsAccepted() {
        //sync();
        go();
    }

    private void go() {
        String to_apply = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext()).getString("preference_set_locale","");
        if(!to_apply.equals("")) {
            Locale myLocale = new Locale(to_apply);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        }
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }

    private void sync(){
        Intent msgIntent = new Intent(this, SyncService.class);
        startService(msgIntent);
    }
}
