package eu.fbk.mpba.app.fruitipy.database;

import android.provider.BaseColumns;

/**
 * Created by ballerin on 12/7/16.
 */

public final class DatabaseContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private DatabaseContract() {}

    /* Inner class that defines the table contents */
    public static class Strings implements BaseColumns {
        public static final String TABLE_NAME = "samples";
        public static final String COLUMN_NAME_SPECTRUM = "spectrum";
        public static final String COLUMN_NAME_FRUIT = "fruit";
        public static final String COLUMN_NAME_GPS = "gps";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_RESULT = "result";
        public static final String COLUMN_NAME_IS_CORRECT = "correct";
        public static final String COLUMN_NAME_NOTES = "notes";
        public static final String COLUMN_NAME_UNIVERSAL_ID = "universal_id";
        public static final String COLUMN_NAME_TO_UPDATE = "send";
        public static final String COLUMN_NAME_DATE = "date";

        public static final String PHOTOS_TABLE_NAME = "photos";
        public static final String COLUMN_NAME_PATH = "path";


        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        COLUMN_NAME_SPECTRUM + " TEXT, " +
                        COLUMN_NAME_FRUIT + " TEXT, " +
                        COLUMN_NAME_GPS + " TEXT, " +
                        COLUMN_NAME_TIME + " TEXT, " +
                        COLUMN_NAME_RESULT + " TEXT, " +
                        COLUMN_NAME_IS_CORRECT + " INTEGER, " +
                        COLUMN_NAME_NOTES + " TEXT, " +
                        COLUMN_NAME_UNIVERSAL_ID + " INTEGER, " +
                        COLUMN_NAME_TO_UPDATE + " INTEGER" + " )";

        public static final String SQL_CREATE_PHOTOS =
                "CREATE TABLE " + PHOTOS_TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        COLUMN_NAME_TO_UPDATE + " INTEGER, " +
                        COLUMN_NAME_FRUIT + " TEXT, " +
                        COLUMN_NAME_DATE + " TEXT, " +
                        COLUMN_NAME_TIME + " TEXT, " +
                        COLUMN_NAME_PATH + " TEXT" + " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static final String SQL_DELETE_PHOTOS =
                "DROP TABLE IF EXISTS " + PHOTOS_TABLE_NAME;
    }
}


