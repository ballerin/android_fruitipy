package eu.fbk.mpba.app.fruitipy.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.activities.GoogleMapsActivity;
import eu.fbk.mpba.app.fruitipy.R;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.Locale;

/**
 * Created by ballerin on 17/03/17.
 */

public class SettingsFragment extends PreferenceFragment{

    private String TAG = "SettingsFragment";
    private Preference mapPref;
    private Preference localePref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        mapPref = (Preference) findPreference("preference_set_fake_position");
        mapPref.setOnPreferenceClickListener (new Preference.OnPreferenceClickListener(){
            @Override
            public boolean onPreferenceClick(Preference preference) {
                //Toast.makeText(Fruitipy.getAppContext(), "PRESSED", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getContext(), GoogleMapsActivity.class);
                int PLACE_PICKER_REQUEST = 0;
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                    /*
                      *If the activity interrupts without errors the API could be not enabled for Google Places for Android
                      *
                      * Check on http://console.developers.google.com
                     */
                }
                catch (Exception e){

                }

                //startActivityForResult(i, 0);
                return true;
            }
        });

        localePref = (Preference) findPreference("preference_set_locale");
        localePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if(o.toString().equals("system")){
                    Locale myLocale = Locale.getDefault();
                    Toast.makeText(Fruitipy.getAppContext(), "Locale changed to " + myLocale.getDisplayName(), Toast.LENGTH_SHORT).show();
                    Resources res = getResources();
                    DisplayMetrics dm = res.getDisplayMetrics();
                    Configuration conf = res.getConfiguration();
                    conf.locale = myLocale;
                    res.updateConfiguration(conf, dm);
                }
                else {
                    Locale myLocale = new Locale(o.toString());
                    Toast.makeText(Fruitipy.getAppContext(), "Locale changed to " + myLocale.getDisplayName(), Toast.LENGTH_SHORT).show();
                    Resources res = getResources();
                    DisplayMetrics dm = res.getDisplayMetrics();
                    Configuration conf = res.getConfiguration();
                    conf.locale = myLocale;
                    res.updateConfiguration(conf, dm);

                    getActivity().recreate();
                }
                return true;

            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case 0:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    Place place = PlacePicker.getPlace(data, getActivity());
                    String latitude = String.valueOf(place.getLatLng().latitude);
                    String longitude = String.valueOf(place.getLatLng().longitude);
                    String position = latitude + "," + longitude;
                    String toastMsg = String.format("Place: %s", position);
                    PreferenceManager.getDefaultSharedPreferences(this.getContext()).edit().putString("preference_set_fake_position", position).apply();
                    Toast.makeText(Fruitipy.getAppContext(), toastMsg, Toast.LENGTH_LONG).show();

                }
                break;
            default:
                Log.e(TAG, "wrong request code");
                break;
        }
    }
}

