package eu.fbk.mpba.app.fruitipy.models;

/**
 * Created by ballerin on 11/25/16.
 */

public class Spectrum {
    private long id = -1;
    private int n = 288; //Number of values per sample
    private double[] spectrum = null;
    private String spectrum_as_string = null;
    private double gps_lat = 0;
    private double gps_long = 0;
    private long time = 0;
    private String fruit;
    private String result;
    private int isCorrect;
    private String notes;
    private int universal;

    public Spectrum(){}

    public void setSpectrum(double[] Data){
        if(Data.length == 0) {
            throw new IllegalArgumentException();
        }
        this.spectrum = Data;
    }

    public double[] getSpectrum(){
        return  spectrum;
    }

    public void setGps(double lati, double longi){
        gps_lat = lati;
        gps_long = longi;
    }

    public double[] getGps(){
        double[] gps = {gps_lat, gps_long};
        return gps;
    }

    public void setTime(long unix_timestamp){
        time = unix_timestamp;
    }

    public long getTime(){
        return time;
    }

    public String spectrumAsString(){
        if(spectrum_as_string == null){
            spectrum_as_string = "";
            for(int i=0; i<spectrum.length;i++){
                spectrum_as_string += String.valueOf(spectrum[i]) + ",";
            }
            spectrum_as_string += String.valueOf(spectrum[spectrum.length-1]);
        }
        return spectrum_as_string;
    }

    public String gpsAsString(){
        if(gps_lat != 0.0 || gps_long != 0.0) {
            return Double.valueOf(gps_lat).toString() + "," + Double.valueOf(gps_long).toString();
        }
        else{
            return "n/d";
        }
    }

    public void setId(long idToSet){
        id = idToSet;
    }

    public long getId(){
        return id;
    }

    public void setFruit(String f){
        fruit = f;
    }

    public String getFruit(){
        return fruit;
    }

    public void setResult(String r){
        result = r;
    }

    public String getResult() {
        return result;
    }

    public void setCorrect(int c){
        isCorrect = c;
    }

    public int getCorrect() {
        return isCorrect;
    }

    public void setNotes(String n) {
        notes = n;
    }

    public String getNotes() {
        return notes;
    }

    public void setUni(int uni) {
        universal = uni;
    }

    public Integer getUni() {
        return universal;
    }
}