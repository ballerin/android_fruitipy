package eu.fbk.mpba.app.fruitipy.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.Hooks;
import eu.fbk.mpba.app.fruitipy.location.LocationCallbacks;
import eu.fbk.mpba.app.fruitipy.location.LocationHelper;
import eu.fbk.mpba.app.fruitipy.location.LocationObserver;
import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.adapters.AggregatedSamplesAdapter;
import eu.fbk.mpba.app.fruitipy.bluetooth.BLEObserver;
import eu.fbk.mpba.app.fruitipy.bluetooth.UARTCallbacks;
import eu.fbk.mpba.app.fruitipy.database.DatabaseContract;
import eu.fbk.mpba.app.fruitipy.database.DbUtils;
import eu.fbk.mpba.app.fruitipy.fragments.SelectFruit;
import eu.fbk.mpba.app.fruitipy.interfaces.CustomDialogInterface;
import eu.fbk.mpba.app.fruitipy.models.AggregatedSample;
import eu.fbk.mpba.app.fruitipy.models.AggregatedSampleSet;
import eu.fbk.mpba.app.fruitipy.models.Photo;
import eu.fbk.mpba.app.fruitipy.models.PhotoHelper;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static eu.fbk.mpba.app.fruitipy.utils.Utils.getDb;

/*
 * Created by ballerin on 2/8/17.
 *
 * This activity is the main activity
 *
 * From here the user can access all the data saved in the device
 */

public class MainActivity extends AppCompatActivity implements Hooks.Received, Hooks.Progress, CustomDialogInterface, UARTCallbacks, LocationCallbacks{

    public static List<AggregatedSample> samples = new ArrayList<>();
    private AggregatedSampleSet agSet = new AggregatedSampleSet();
    public static List<Photo> photos = new ArrayList<>();
    private RecyclerView spectraRecyclerView;
    public static AggregatedSamplesAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    SQLiteDatabase db;
    private LocationManager locationManager;
    public static Location location = null;
    private TextView bottomBanner;
    private FloatingActionMenu select_menu;
    private Button select_layer;
    private Locale currentLocale = null;
    private int scan_n = 0;
    private ScheduledExecutorService exec;

    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_NEW_SAMPLE = 3;
    private static final int REQUEST_NEW_PHOTO = 4;
    private static final int REQUEST_SHOW_PHOTO = 5;
    private static final int UART_PROFILE_READY = 10;
    public static final String TAG = "MAIN";
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;

    private boolean on;
    public String fruitSelected = "";
    public Boolean takePhoto = false;
    private int err_uart = 0;


    TextView mRemoteRssiVal;
    RadioGroup mRg;
    private int mState = UART_PROFILE_DISCONNECTED;
    private BluetoothAdapter mBtAdapter = null;
    private ListView messageListView;
    private ArrayAdapter<String> listAdapter;
    private FloatingActionButton newSampleButton;
    private FloatingActionButton bluetoothButton;
    private Button btnSend;
    private EditText edtMessage;
    private ProgressDialog progress;
    private static Activity activity;
    private Timer tt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = this;

        setContentView(R.layout.activity_aggregated_samples_list);
        currentLocale = getResources().getConfiguration().locale;
        ButterKnife.bind(this);

        bottomBanner = (TextView) findViewById(R.id.bottom_banner);
        spectraRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new AggregatedSamplesAdapter(samples);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        spectraRecyclerView.setLayoutManager(mLayoutManager);
        spectraRecyclerView.setItemAnimator(new DefaultItemAnimator());
        spectraRecyclerView.setAdapter(mAdapter);

        updateSamplesList();


        //Init bluetooth
        BLEObserver.getInstance().subscribe(this); //Subscribe to the receiver
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_not_available), Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        newSampleButton = (FloatingActionButton) findViewById(R.id.floating_new_sample);
        bluetoothButton = (FloatingActionButton) findViewById(R.id.floating_bluetooth_connect);


        if(BLEObserver.getReceiver().connectedToBluetooth){
           changeButtonsColor(1);
        }


        Hooks.setOnDisconnect(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (on)
                            onBackPressed();
                    }
                });
            }
        });

        Hooks.setOnReceived(this);
        Hooks.setOnProgress(this);

        select_menu = (FloatingActionMenu) findViewById(R.id.floating_action_menu);
        select_menu.setClosedOnTouchOutside(false);
        select_layer = (Button) findViewById(R.id.select_layer);
        select_menu.setOnMenuButtonClickListener(menuButtonClickListener);
        select_layer.setOnClickListener(selectLayerClickListener);

    }

    @Override
    protected void onStart() {
        super.onStart();
        LocationObserver.getInstance().subscribe(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        LocationObserver.getInstance().subscribe(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        updateSamplesList(); //Just to refresh the background
        if(!select_menu.isOpened()){
            select_layer.setVisibility(View.GONE);
        }

        if(currentLocale != getResources().getConfiguration().locale){
            recreate();
        }

        if(BLEObserver.getInstance().isConnected()) {
            changeButtonsColor(1);
        }
        else{
            changeButtonsColor(0);
        }

        //When resumed check if the token is still valid. If not remove it and force new login
        SharedPreferences prefs = getSharedPreferences("saved_token", MODE_PRIVATE);
        String token = prefs.getString("token", null);

        if(token == null || token.equals("")){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");

        //Unregister uart service
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(BLEObserver.getReceiver());
        } catch (Exception ignore) {
            Log.e(TAG, ignore.toString());
        }
        BLEObserver.getInstance().disconnect();
        BLEObserver.getInstance().unsubscribe(this);
        Hooks.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;
            case R.id.logout:
                SharedPreferences.Editor editor = getSharedPreferences("saved_token", MODE_PRIVATE).edit();
                editor.remove("token");
                editor.commit();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    BLEObserver.getInstance().connectTo(deviceAddress);
                    //((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName()+ " - connecting");

                }
                break;

            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, getString(R.string.bluetooth_has_been_turned_on), Toast.LENGTH_SHORT).show();

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;

            case REQUEST_NEW_SAMPLE:
                if (resultCode == Activity.RESULT_OK && BLEObserver.getReceiver().connectedToBluetooth){
                    okButtonClicked(fruitSelected, "spectrum");
                }
                break;

            case REQUEST_NEW_PHOTO:
                updateSamplesList();
                select_layer.setVisibility(View.GONE);
                Intent i = new Intent(Fruitipy.getAppContext(), ShowPhotoActivity.class);
                i.putExtra("PHOTO_PATH", data.getData().toString());
                startActivityForResult(i, REQUEST_SHOW_PHOTO);
                break;

            case REQUEST_SHOW_PHOTO:
                if (resultCode == Activity.RESULT_OK){
                    okButtonClicked(fruitSelected, "photo");
                }
                break;

            default:
                Log.e(TAG, "wrong request code");
                break;


        }
    }

    public void changeButtonsColor(int state){
        switch(state) {
            case 1:
                newSampleButton.setColorNormal(getResources().getColor(R.color.fab_color_normal));
                newSampleButton.setColorPressed(getResources().getColor(R.color.fab_color_pressed));
                newSampleButton.setColorRipple(getResources().getColor(R.color.fab_color_ripple));

                bluetoothButton.setColorNormal(getResources().getColor(R.color.bluetooth_green));
                bluetoothButton.setColorPressed(getResources().getColor(R.color.bluetooth_green));
                bluetoothButton.setColorRipple(getResources().getColor(R.color.fab_color_ripple));
                bluetoothButton.setLabelText(getString(R.string.fab_bluetooth_disconnect_label));

                break;

            case 0:
                newSampleButton.setColorNormal(getResources().getColor(R.color.disabled_new_sample));
                newSampleButton.setColorPressed(getResources().getColor(R.color.disabled_new_sample));
                newSampleButton.setColorRipple(getResources().getColor(R.color.disabled_new_sample));

                bluetoothButton.setColorNormal(getResources().getColor(R.color.fab_color_normal));
                bluetoothButton.setColorPressed(getResources().getColor(R.color.fab_color_pressed));
                bluetoothButton.setColorRipple(getResources().getColor(R.color.fab_color_ripple));
                bluetoothButton.setLabelText(getString(R.string.fab_bluetooth_connect_label));

                break;

            default:
                break;
        }
    }

    @OnClick(R.id.floating_new_sample)
    public void floatingNewSample(){
        /*
        When the button to acquire a new sample is pressed 2 things may happen:
         - if the device is not connected then pop-up a fragment with the bluetooth devices available
         - if the device is connected then pop-up a fragment requesting the fruit and then start the acquisition
         */
        if(BLEObserver.getReceiver().connectedToBluetooth) {
            showDialog("spectrum");
            select_menu.close(true);
        }
    }

    @OnClick(R.id.floating_take_photo)
    public void floatingTakePhoto(){
        showDialog("photo");
        select_menu.close(true);
    }

    @OnClick(R.id.shitbutton) //TODO: This must not to go to production
    public void shitbutton(){ //For testing purposes only
        BLEObserver.getInstance().send("R");
    }

    @OnClick(R.id.floating_bluetooth_connect)
    public void floatingBluetoothConnectDisconnect(){
        if(BLEObserver.getReceiver().connectedToBluetooth){
            BLEObserver.getInstance().disconnect();
        }
        else{
            if (!mBtAdapter.isEnabled()) {
                Log.i(TAG, "onClick - BT not enabled yet");
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            }
            else {
                if (!BLEObserver.getReceiver().connectedToBluetooth){

                    //Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices

                    Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                    startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
                } else {
                    //Disconnect button pressed
                    BLEObserver.getInstance().disconnect();

                }
            }
        }
    }

    public void sampleSelected() {
        select_menu.close(true);
    }

    public void updateSamplesList(){
        /*
        This function creates updates the recyclerView adding all the aggregatedSamples saved in the db
        It also calculates (and updates) the counter of taken samples
         */

        samples.clear();

        db = getDb();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) as count,fruit,strftime('%d-%m-%Y',datetime("+DatabaseContract.Strings.COLUMN_NAME_TIME+",'unixepoch')) as day"+
                " FROM " + DatabaseContract.Strings.TABLE_NAME +
                " GROUP BY day, "+DatabaseContract.Strings.COLUMN_NAME_FRUIT +
                " ORDER BY "+DatabaseContract.Strings.COLUMN_NAME_TIME+" DESC", null);
        int count = cursor.getCount();
        // Defined Array values to show in ListView
        do{
            cursor.moveToNext();
            if(!cursor.isAfterLast()) {
                AggregatedSample ag = new AggregatedSample();
                ag.setFruit(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_FRUIT)));
                ag.setDate(cursor.getString(cursor.getColumnIndexOrThrow("day")));
                ag.setCount(cursor.getInt(cursor.getColumnIndexOrThrow("count")));
                samples.add(ag);
            }
        }while(!cursor.isLast() && !cursor.isAfterLast());

        photos = PhotoHelper.retrieveAggregatedPhotos();
        for(int i=0;i<photos.size();i++){
            AggregatedSample ag =new AggregatedSample();
            ag.setFruit(PhotoHelper.getFruitFromPath(photos.get(i).getPath()));
            ag.setDate(PhotoHelper.getDateFromPath(photos.get(i).getPath()));
            ag.setCount(0);
            if(!samples.contains(ag)){
                samples.add(ag);
            }
        }
        if(samples.size()>0 || photos.size()>0){
            spectraRecyclerView.setBackgroundResource(0);
        }
        mAdapter.notifyDataSetChanged();
    }

    public void showDialog(String sample){
        Bundle args = new Bundle();
        args.putString("last_fruit", fruitSelected);
        args.putString("sample_type", sample);
        SelectFruit newFragment = new SelectFruit();
        newFragment.setArguments(args);
        newFragment.show(getFragmentManager(), "dialog");
    }

    @Override
    public void okButtonClicked(String fruit, String type){
        /*
        The acquisition has been requested. This function shows the progressDialog and starts the acquisition
         */
        fruitSelected = fruit;

        if(type.equals("spectrum")) {
            scan_n++;
            progress = ProgressDialog.show(this, "Data acquisition...", "Scanning...", false);

            /*
            This is just a way to ensure everything is proceeding good
            If the data stream is not completed in n (arbitrary number I decided) seconds
            then the process is aborted and a Toast is showed
             */
            tt = new Timer();
            tt.schedule(getDisconnectionTimer(scan_n),5000);
            Hooks.acquire();

        }
        else if(type.equals("photo")){
            Intent photoIntent = new Intent(MainActivity.this, TakePhotoActivity.class);
            photoIntent.putExtra("FRUIT", fruitSelected);
            photoIntent.putExtra("LOCATION", composeLocationString(location,"x"));
            startActivityForResult(photoIntent, REQUEST_NEW_PHOTO);
        }


        //return generateDetailActivityIntent(newRowId,fruits[fruit]);
    }

    public Intent generateDetailActivityIntent(long id, String fruit_string){
        /*
        Given the id of the sample a new activity showing more info is started
         */
        Intent i = new Intent(Fruitipy.getAppContext(), DetailActivity.class);
        i.putExtra("SAMPLE_ID", id);
        i.putExtra("FRUIT", fruit_string);
        return i;
    }

    @Override
    public void acquisitionProgress(int num, int outof) {
        /*
        This function updates the value in the progressbar.
        TODO: change name and make this idiotproof
         */

        Log.i("Progress", "num: " + num + ", outof: " + outof);

        tt.cancel();tt=new Timer();
        tt.schedule(getDisconnectionTimer(scan_n),3000);

        if(progress==null){
            progress = ProgressDialog.show(this, "Data acquisition...", "Scanning...", false);
        }

        if(outof != 0) {
            int progressPercentage = num*100/outof;
            if(progressPercentage > 100){
                progressPercentage = 100;
            }
            progress.setMessage(String.valueOf(progressPercentage) + "%");
        }
        else {
            progress.setMessage(getString(R.string.division_by_zero_error));
        }
    }

    @Override
    public void acquisitionFinished(String text, final char code, final int[] valori, final int length) {
        /*
        This function is called when the acquisition has finished. The location is retrieved and the sample is saved.
        TODO: change name
         */
        tt.cancel();
        progress.dismiss();
        String sSpectrum = "";
        for(int i=0; i<length-1; i++){
            sSpectrum += String.valueOf(valori[i])/*.substring(0,3) */+ ",";
        }
        sSpectrum += String.valueOf(valori[length-1])/*.substring(0,3)*/;



        long newRowId = DbUtils.insertDataInDb(fruitSelected, composeLocationString(location, "/"), sSpectrum);

        //Toast.makeText(Fruitipy.getAppContext(), "New insertion in DB: " + String.valueOf(newRowId), Toast.LENGTH_SHORT).show();

        startActivityForResult(generateDetailActivityIntent(newRowId, String.valueOf(fruitSelected)), REQUEST_NEW_SAMPLE);
    }

    private String composeLocationString(Location location, String delimiter){
        //Get last location
        String loc_string = "";
        try {
            loc_string += String.valueOf(new DecimalFormat("#.####").format(location.getLatitude())).replace(",",".") + delimiter + String.valueOf(new DecimalFormat("#.####").format(location.getLongitude())).replace(",",".");
        }
        catch (Exception e ){
            Toast.makeText(Fruitipy.getAppContext(), e.toString(), Toast.LENGTH_LONG).show();
            loc_string = "";
        }

        return loc_string;
    }

    public void showMessage(String msg) {
        /*
        Just to reduce the code
         */
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    /*
     * LISTENERS
     */

    View.OnClickListener menuButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!select_menu.isOpened()){
                select_layer.setVisibility(View.VISIBLE);
            }
            else{
                select_layer.setVisibility(View.GONE);
            }
            select_menu.toggle(true);
        }
    };
    View.OnClickListener selectLayerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(select_menu.isOpened()) {
                select_menu.toggle(true);
            }
            select_layer.setVisibility(View.GONE);
        }
    };

    /*
     * CALLBACKS
     */

    @Override
    public void onDeviceConnected(){
        changeButtonsColor(1);

        mState = UART_PROFILE_CONNECTED;
        Hooks.connected(this);
    }

    @Override
    public void onDeviceDisconnected(){
        changeButtonsColor(0);

        mState = UART_PROFILE_DISCONNECTED;


        Hooks.disconnected(this);
    }

    @Override
    public void onConnectionError(){
        progress.dismiss();
    }

    private TimerTask getDisconnectionTimer(int scan_now) {
        return new TimerTask() {
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progress.isShowing() && scan_now == scan_n) {
                            progress.dismiss();
                            Log.d(TAG, "Disconnected, scan aborted");
                            Toast.makeText(Fruitipy.getAppContext(), getString(R.string.reconnect_to_device), Toast.LENGTH_LONG).show();
                            select_layer.setVisibility(View.GONE);
                            BLEObserver.getReceiver().connectedToBluetooth = false;
                            changeButtonsColor(0);
                            mState = UART_PROFILE_DISCONNECTED;
                            BLEObserver.getInstance().disconnect();
                            Hooks.disconnected(MainActivity.this);
                        } else {
                            Log.d(TAG, "There is nothing to abort");
                            //Toast.makeText(Fruitipy.getAppContext(), "NOT STOPPED", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        };
    }

    @Override
    public void onLocationUpdated(Location l, boolean m, boolean e){
        location = l;
        LocationHelper.onLocationUpdated(l,m,e,this,bottomBanner);
    }

}
