package eu.fbk.mpba.app.fruitipy.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import eu.fbk.mpba.app.fruitipy.utils.Utils;
import eu.fbk.mpba.app.fruitipy.models.PhotoHelper;

import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_DATE;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_FRUIT;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_GPS;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_IS_CORRECT;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_NOTES;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_PATH;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_SPECTRUM;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_TIME;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_TO_UPDATE;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.COLUMN_NAME_UNIVERSAL_ID;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.PHOTOS_TABLE_NAME;
import static eu.fbk.mpba.app.fruitipy.database.DatabaseContract.Strings.TABLE_NAME;

/**
 * Created by ballerin on 10/12/17.
 */

public class DbUtils {
    public static long insertDataInDb(String fruit, String loc_string, String data){
        /*
        Given the fruit, the location (lat,long) and the data (string of values) insert a new row in the db
         */

        // Gets the data repository in write mode
        SQLiteDatabase db = Utils.getDb();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_SPECTRUM, data);
        values.put(COLUMN_NAME_FRUIT, fruit);
        values.put(COLUMN_NAME_GPS, loc_string);
        values.put(COLUMN_NAME_TIME, Long.valueOf(System.currentTimeMillis() / 1000L).toString());
        values.put(COLUMN_NAME_IS_CORRECT, -1);
        values.put(COLUMN_NAME_NOTES, "");
        values.put(COLUMN_NAME_TO_UPDATE, 1);
        values.put(COLUMN_NAME_UNIVERSAL_ID,-1);

        // Insert the new row, returning the primary key value of the new row
        return db.insert(TABLE_NAME, null, values);
    }

    public static long insertPhotoRefInDb(String ref){
        /*
        Given the path of the photo insert a new row in the db
         */

        // Gets the data repository in write mode
        SQLiteDatabase db = Utils.getDb();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_PATH, ref);
        values.put(COLUMN_NAME_FRUIT, PhotoHelper.getFruitFromPath(ref));
        values.put(COLUMN_NAME_DATE, PhotoHelper.getReversedDateFromPath(ref));
        values.put(COLUMN_NAME_TIME, PhotoHelper.getTimeFromPath(ref));
        values.put(COLUMN_NAME_TO_UPDATE, 1);

        // Insert the new row, returning the primary key value of the new row
        return db.insert(PHOTOS_TABLE_NAME, null, values);
    }

    public static long howManyPhotosInDb(){

        // Gets the data repository in write mode
        SQLiteDatabase db = Utils.getDb();

        String[] projection = {
                ,
        };

        String selection = DatabaseContract.Strings.COLUMN_NAME_PATH + " != ?";


        String[] selectionArgs = {"1"};

        Cursor countRow = db.query(
                DatabaseContract.Strings.PHOTOS_TABLE_NAME,     // The table to query
                projection,                                     // The columns to return
                selection,                                      // The columns for the WHERE clause
                selectionArgs,                                  // The values for the WHERE clause
                null,                                   // don't group the rows
                null,                                    // don't filter by row groups
                null                                    // The sort order
        );
        int count = countRow.getCount();
        countRow.close();
        //Log.d(TAG, "There are " + String.valueOf(count) + " rows to sync");
        return count;
    }

    public static long howManyPhotosToSyncInDb(){

        // Gets the data repository in write mode
        SQLiteDatabase db = Utils.getDb();

        String[] projection = {
                ,
        };

        String selection = DatabaseContract.Strings.COLUMN_NAME_TO_UPDATE + " = ?";


        String[] selectionArgs = {"1"};

        Cursor countRow = db.query(
                DatabaseContract.Strings.PHOTOS_TABLE_NAME,     // The table to query
                projection,                                     // The columns to return
                selection,                                      // The columns for the WHERE clause
                selectionArgs,                                  // The values for the WHERE clause
                null,                                   // don't group the rows
                null,                                    // don't filter by row groups
                null                                    // The sort order
        );
        int count = countRow.getCount();
        countRow.close();
        //Log.d(TAG, "There are " + String.valueOf(count) + " rows to sync");
        return count;
    }

    public static boolean photoAlreadySynced(String ref){

        // Gets the data repository in write mode
        SQLiteDatabase db = Utils.getDb();

        String[] projection = {
                ,
        };

        String selection = DatabaseContract.Strings.COLUMN_NAME_PATH + " = ?";


        String[] selectionArgs = {ref};

        Cursor countRow = db.query(
                DatabaseContract.Strings.PHOTOS_TABLE_NAME,     // The table to query
                projection,                                     // The columns to return
                selection,                                      // The columns for the WHERE clause
                selectionArgs,                                  // The values for the WHERE clause
                null,                                   // don't group the rows
                null,                                    // don't filter by row groups
                null                                    // The sort order
        );
        int count = countRow.getCount();
        countRow.close();
        //Log.d(TAG, "There are " + String.valueOf(count) + " rows to sync");
        return count > 0;
    }
}
