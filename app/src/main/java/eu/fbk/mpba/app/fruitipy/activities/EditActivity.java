package eu.fbk.mpba.app.fruitipy.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import eu.fbk.mpba.app.fruitipy.database.DatabaseContract;
import eu.fbk.mpba.app.fruitipy.models.Spectrum;
import eu.fbk.mpba.app.fruitipy.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static eu.fbk.mpba.app.fruitipy.utils.Utils.getDb;
import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.convertTimestampToReadableFormat;
import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.getSpectrumById;

/**
 * Created by ballerin on 2/7/17.
 */

public class EditActivity extends AppCompatActivity{

    static String TAG = "EditActivity";

    Spectrum sample;

    TextView id;
    TextView date;
    TextView gps;
    TextView result;
    TextView notes;
    Spinner spinner;

    SQLiteDatabase db;

    MapView mapView;
    GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        ButterKnife.bind(this);

        sample = getSpectrumById(getIntent().getExtras().getLong("SAMPLE_ID"));

        id = (TextView) findViewById(R.id.id_desc);
        date = (TextView) findViewById(R.id.edit_date);
        gps = (TextView) findViewById(R.id.edit_gps);
        result = (TextView) findViewById(R.id.edit_result);
        notes = (TextView) findViewById(R.id.edit_notes);

        id.setText(String.valueOf(getIntent().getExtras().getLong("SAMPLE_ID")));
        date.setText(convertTimestampToReadableFormat(sample.getTime()));
        gps.setText(sample.gpsAsString());
        result.setText(sample.getResult());

        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.correctness_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        if(sample.getCorrect() != -1){
            spinner.setSelection(sample.getCorrect()+1);
        }

        if(sample.getNotes() != null){
            notes.setText(sample.getNotes());
        }

        mapView = findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        LatLng gpsPosition = new LatLng(sample.getGps()[0], sample.getGps()[1]);


        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(gpsPosition, 12);

        mapView.getMapAsync(new OnMapReadyCallback(){
            @Override
            public void onMapReady(GoogleMap map){
                Log.d(TAG, "Map Ready");
                map.animateCamera(cameraUpdate);
                map.addMarker(new MarkerOptions().position(gpsPosition).draggable(false).visible(true));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            case R.id.save_button:
                db = getDb();
                ContentValues values = new ContentValues();
                values.put(DatabaseContract.Strings.COLUMN_NAME_IS_CORRECT,spinner.getSelectedItemId()-1);
                values.put(DatabaseContract.Strings.COLUMN_NAME_NOTES,String.valueOf(notes.getText()));
                values.put(DatabaseContract.Strings.COLUMN_NAME_TO_UPDATE,1);

                String selection = DatabaseContract.Strings._ID + " = ?";
                String[] selectionArgs = {String.valueOf(sample.getId())};

                db.update(DatabaseContract.Strings.TABLE_NAME,values,selection,selectionArgs);

                this.finish();

        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.linearLayout)
    public void showGPSOnMap() {
        Uri gmmIntentUri = Uri.parse("geo:" + sample.gpsAsString() + "?q=" + sample.gpsAsString() + getString(R.string.registration_point));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        startActivity(mapIntent);
    }


}