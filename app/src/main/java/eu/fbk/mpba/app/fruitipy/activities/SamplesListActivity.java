package eu.fbk.mpba.app.fruitipy.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.Hooks;
import eu.fbk.mpba.app.fruitipy.location.LocationCallbacks;
import eu.fbk.mpba.app.fruitipy.location.LocationHelper;
import eu.fbk.mpba.app.fruitipy.location.LocationObserver;
import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.adapters.PhotosAdapter;
import eu.fbk.mpba.app.fruitipy.adapters.SamplesAdapter;
import eu.fbk.mpba.app.fruitipy.bluetooth.BLEObserver;
import eu.fbk.mpba.app.fruitipy.bluetooth.UARTCallbacks;
import eu.fbk.mpba.app.fruitipy.database.DatabaseContract;
import eu.fbk.mpba.app.fruitipy.database.DbUtils;
import eu.fbk.mpba.app.fruitipy.fragments.SelectFruit;
import eu.fbk.mpba.app.fruitipy.interfaces.CustomDialogInterface;
import eu.fbk.mpba.app.fruitipy.models.Photo;
import eu.fbk.mpba.app.fruitipy.models.PhotoHelper;
import eu.fbk.mpba.app.fruitipy.models.Spectrum;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static eu.fbk.mpba.app.fruitipy.utils.Utils.getDb;
import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.getSpectrumById;

/*
 * Created by ballerin on 2/8/17.
 *
 *
 * From here the user can access all the data saved in the device organized by date and fruit
 *
 */

public class SamplesListActivity extends AppCompatActivity implements Hooks.Received, Hooks.Progress, CustomDialogInterface, UARTCallbacks, LocationCallbacks{

    private List<Spectrum> samples = new ArrayList<>();
    private List<Photo> photos = new ArrayList<>();
    private RecyclerView spectraRecyclerView;
    private RecyclerView photosRecyclerView;
    public static PhotosAdapter photosAdapter;
    public static SamplesAdapter mAdapter;
    SQLiteDatabase db;
    private LocationManager locationManager;
    private Location location = null;
    private TextView bottomBanner;
    private TabLayout tabLayout;
    private FloatingActionMenu select_menu;
    private Button select_layer;
    private Locale currentLocale = null;
    private int scan_n = 0;

    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_NEW_SAMPLE = 3;
    private static final int REQUEST_NEW_PHOTO = 4;
    private static final int REQUEST_SHOW_PHOTO = 5;
    private static final int UART_PROFILE_READY = 10;
    public static final String TAG = "SLA";
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;

    private boolean on;
    public String fruitSelected = "";
    public Boolean takePhoto = false;
    private int err_uart = 0;


    TextView mRemoteRssiVal;
    RadioGroup mRg;
    private int mState = UART_PROFILE_DISCONNECTED;
    private BluetoothAdapter mBtAdapter = null;
    private ListView messageListView;
    private ArrayAdapter<String> listAdapter;
    private FloatingActionButton newSampleButton;
    private FloatingActionButton BluetoothButton;
    private Button btnSend;
    private EditText edtMessage;
    private ProgressDialog progress;
    private static Activity activity;

    private String currentDate;
    private String currentFruit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = this;

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        currentDate = this.getIntent().getExtras().getString("DATE");
        currentFruit = this.getIntent().getExtras().getString("FRUIT");

        setContentView(R.layout.activity_samples_list);
        currentLocale = getResources().getConfiguration().locale;
        ButterKnife.bind(this);

        bottomBanner = (TextView) findViewById(R.id.bottom_banner);

        //Init bluetooth
        BLEObserver.getInstance().subscribe(this); //Subscribe to the receiver
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_not_available), Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        newSampleButton = (FloatingActionButton) findViewById(R.id.floating_new_sample);
        BluetoothButton = (FloatingActionButton) findViewById(R.id.floating_bluetooth_connect);

        if(BLEObserver.getReceiver().connectedToBluetooth){
            changeButtonsColor(1);
        }

        Hooks.setOnReceived(this);
        Hooks.setOnProgress(this);

        select_menu = (FloatingActionMenu) findViewById(R.id.floating_action_menu);
        select_menu.setClosedOnTouchOutside(false);
        select_layer = (Button) findViewById(R.id.select_layer);
        select_menu.setOnMenuButtonClickListener(menuButtonClickListener);
        select_layer.setOnClickListener(selectLayerClickListener);

        photosRecyclerView = (RecyclerView) findViewById(R.id.photos_recycler_view);

        tabLayout = (TabLayout) findViewById(R.id.TypeSelect);
        tabLayout.addOnTabSelectedListener(tabSelectedListener);

        updateSamplesList(); //Needs to be here AND in the onResume
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocationObserver.getInstance().subscribe(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        LocationObserver.getInstance().unsubscribe(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        updateSamplesList(); //Just to refresh the background
        if(!select_menu.isOpened()){
            select_layer.setVisibility(View.GONE);
        }

        if(currentLocale != getResources().getConfiguration().locale){
            recreate();
        }

        //When resumed check if the token is still valid. If not remove it and force new login
        SharedPreferences prefs = getSharedPreferences("saved_token", MODE_PRIVATE);
        String token = prefs.getString("token", null);

        if(token == null || token.equals("")){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;
            case R.id.logout:
                SharedPreferences.Editor editor = getSharedPreferences("saved_token", MODE_PRIVATE).edit();
                editor.remove("token");
                editor.commit();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    BLEObserver.getInstance().connectTo(deviceAddress);
                }
                break;

            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, getString(R.string.bluetooth_has_been_turned_on), Toast.LENGTH_SHORT).show();

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;

            case REQUEST_NEW_SAMPLE:
                if (resultCode == Activity.RESULT_OK && BLEObserver.getReceiver().connectedToBluetooth){
                    okButtonClicked(fruitSelected, "spectrum");
                }
                break;

            case REQUEST_NEW_PHOTO:
                //updateSamplesList();
                //photosRecyclerView.scrollToPosition(photos.size()-1);
                select_layer.setVisibility(View.GONE);
                Intent i = new Intent(Fruitipy.getAppContext(), ShowPhotoActivity.class);
                i.putExtra("PHOTO_PATH", data.getData().toString());
                startActivityForResult(i, REQUEST_SHOW_PHOTO);
                break;

            case REQUEST_SHOW_PHOTO:
                if (resultCode == Activity.RESULT_OK){
                    okButtonClicked(fruitSelected, "photo");
                }
                break;

            default:
                Log.e(TAG, "wrong request code");
                break;


        }
    }

    public void changeButtonsColor(int state){
        switch(state) {
            case 1:
                newSampleButton.setColorNormal(getResources().getColor(R.color.fab_color_normal));
                newSampleButton.setColorPressed(getResources().getColor(R.color.fab_color_pressed));
                newSampleButton.setColorRipple(getResources().getColor(R.color.fab_color_ripple));

                BluetoothButton.setColorNormal(getResources().getColor(R.color.bluetooth_green));
                BluetoothButton.setColorPressed(getResources().getColor(R.color.bluetooth_green));
                BluetoothButton.setColorRipple(getResources().getColor(R.color.fab_color_ripple));
                BluetoothButton.setLabelText(getString(R.string.fab_bluetooth_disconnect_label));

                break;

            case 0:
                newSampleButton.setColorNormal(getResources().getColor(R.color.disabled_new_sample));
                newSampleButton.setColorPressed(getResources().getColor(R.color.disabled_new_sample));
                newSampleButton.setColorRipple(getResources().getColor(R.color.disabled_new_sample));

                BluetoothButton.setColorNormal(getResources().getColor(R.color.fab_color_normal));
                BluetoothButton.setColorPressed(getResources().getColor(R.color.fab_color_pressed));
                BluetoothButton.setColorRipple(getResources().getColor(R.color.fab_color_ripple));
                BluetoothButton.setLabelText(getString(R.string.fab_bluetooth_connect_label));

                break;

            default:
                break;
        }
    }

    @OnClick(R.id.floating_new_sample)
    public void floatingNewSample(){
        /*
        When the button to acquire a new sample is pressed 2 things may happen:
         - if the device is not connected then pop-up a fragment with the bluetooth devices available
         - if the device is connected then pop-up a fragment requesting the fruit and then start the acquisition
         */
        if(BLEObserver.getReceiver().connectedToBluetooth) {
            showDialog("spectrum");
            select_menu.close(true);
        }
    }

    @OnClick(R.id.floating_take_photo)
    public void floatingTakePhoto(){
        showDialog("photo");
        select_menu.close(true);
    }

    @OnClick(R.id.shitbutton)
    public void shitbutton(){
        BLEObserver.getInstance().send("R");
    }

    @OnClick(R.id.floating_bluetooth_connect)
    public void floatingBluetoothConnectDisconnect(){
        if(BLEObserver.getReceiver().connectedToBluetooth){
            BLEObserver.getInstance().disconnect();
        }
        else{
            if (!mBtAdapter.isEnabled()) {
                Log.i(TAG, "onClick - BT not enabled yet");
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            }
            else {
                if (!BLEObserver.getReceiver().connectedToBluetooth){

                    //Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices

                    Intent newIntent = new Intent(SamplesListActivity.this, DeviceListActivity.class);
                    startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
                } else {
                    //Disconnect button pressed
                    BLEObserver.getInstance().disconnect();
                }
            }
        }
    }

    public void sampleSelected() {
        select_menu.close(true);
    }

    public void updateSamplesList(){
        /*
        This function creates updates the recyclerView adding all the samples saved in the db
        The WHERE control is not made SQL side but Java side in order to preserve code maintainability.
         */

        samples.clear();

        spectraRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new SamplesAdapter(samples);
        spectraRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        spectraRecyclerView.setItemAnimator(new DefaultItemAnimator());
        spectraRecyclerView.setAdapter(mAdapter);

        photosAdapter = new PhotosAdapter(photos);
        photosRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        photosRecyclerView.setItemAnimator(new DefaultItemAnimator());
        photosRecyclerView.setAdapter(photosAdapter);

        db = getDb();
        Cursor cursor = db.rawQuery("SELECT "+DatabaseContract.Strings._ID+","+DatabaseContract.Strings.COLUMN_NAME_FRUIT+","+DatabaseContract.Strings.COLUMN_NAME_TIME+", strftime('%d-%m-%Y',datetime("+DatabaseContract.Strings.COLUMN_NAME_TIME+",'unixepoch')) as day"+
                " FROM " + DatabaseContract.Strings.TABLE_NAME  +
                " WHERE " + DatabaseContract.Strings.COLUMN_NAME_FRUIT + " = '" + currentFruit + "' AND  day = '" + currentDate +"'", null);
        int count = cursor.getCount();
        // Defined Array values to show in ListView
        do{
            cursor.moveToNext();
            if(!cursor.isAfterLast()) {
                long id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.Strings._ID));
                Spectrum sample = getSpectrumById(id);
                samples.add(sample);
            }
        }while(!cursor.isLast() && !cursor.isAfterLast());
        cursor.close();
        if(samples.size()>0){
            spectraRecyclerView.setBackgroundResource(0);
        }

        mAdapter.notifyDataSetChanged();

        photos = PhotoHelper.retrievePhotosByFruitAndDate(currentFruit, currentDate);
        if(photos.size()>0){
            photosRecyclerView.setBackgroundResource(0);
        }
        photosAdapter.notifyDataSetChanged();
    }

    public void showDialog(String sample){
        Bundle args = new Bundle();
        args.putString("last_fruit", fruitSelected);
        args.putString("sample_type", sample);
        SelectFruit newFragment = new SelectFruit();
        newFragment.setArguments(args);
        newFragment.show(getFragmentManager(), "dialog");
    }

    @Override
    public void okButtonClicked(String fruit, String type){
        /*
        The acquisition has been requested. This function shows the progressDialog and starts the acquisition
         */
        fruitSelected = fruit;

        if(type.equals("spectrum")) {
            scan_n++;
            final int scan_now = scan_n;
            progress = ProgressDialog.show(this, "Data acquisition...", "Scanning...", false);
            final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);

            /*
            This is just a way to ensure everything is proceeding good
            If the data stream is not completed in 8 (arbitrary number I decided) seconds
            then the process is aborted and a Toast is showed
             */
            exec.schedule(new TimerTask(){
                public void run() {
                    activity.runOnUiThread(new Runnable(){
                        @Override
                        public void run(){
                            if(progress.isShowing() && scan_now==scan_n) {
                                progress.dismiss();
                                Log.d(TAG, "Disconnected, scan aborted");
                                Toast.makeText(Fruitipy.getAppContext(), getString(R.string.reconnect_to_device), Toast.LENGTH_LONG).show();
                                select_layer.setVisibility(View.GONE);
                                BLEObserver.getReceiver().connectedToBluetooth = false;
                                changeButtonsColor(0);
                                mState = UART_PROFILE_DISCONNECTED;
                                BLEObserver.getInstance().disconnect();
                                //Hooks.disconnected(SamplesListActivity.this);
                            }
                            else{
                                Log.d(TAG, "There is nothing to abort");
                                //Toast.makeText(Fruitipy.getAppContext(), "NOT STOPPED", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }, 8, TimeUnit.SECONDS); //The integer is empirically chosen (ackhtually quite randomly)
            Hooks.acquire();
        }
        else if(type.equals("photo")){
            Intent photoIntent = new Intent(SamplesListActivity.this, TakePhotoActivity.class);
            photoIntent.putExtra("FRUIT", fruitSelected);
            photoIntent.putExtra("LOCATION", composeLocationString(location,"x"));
            startActivityForResult(photoIntent, REQUEST_NEW_PHOTO);
        }


        //return generateDetailActivityIntent(newRowId,fruits[fruit]);
    }

    public Intent generateDetailActivityIntent(long id, String fruit_string){
        /*
        Given the id of the sample a new activity showing more info is started
         */
        Intent i = new Intent(Fruitipy.getAppContext(), DetailActivity.class);
        i.putExtra("SAMPLE_ID", id);
        i.putExtra("FRUIT", fruit_string);
        return i;
    }

    @Override
    public void acquisitionProgress(int num, int outof) {
        /*
        This function updates the value in the progressbar.
        TODO: change name and make this idiotproof
         */

        Log.i("Progress", "num: " + num + ", outof: " + outof);

        if(progress==null){
            progress = ProgressDialog.show(this, "Data acquisition...", "Scanning...", false);
        }

        if(outof != 0) {
            int progressPercentage = num*100/outof;
            if(progressPercentage > 100){
                progressPercentage = 100;
            }
            progress.setMessage(String.valueOf(progressPercentage) + "%");
        }
        else {
            progress.setMessage(getString(R.string.division_by_zero_error));
        }
    }

    @Override
    public void acquisitionFinished(String text, final char code, final int[] valori, final int length) {
        /*
        This function is called when the acquisition has finished. The location is retrieved and the sample is saved.
         */
        progress.dismiss();
        String sSpectrum = "";
        for(int i=0; i<length-1; i++){
            sSpectrum += String.valueOf(valori[i])/*.substring(0,3) */+ ",";
        }
        sSpectrum += String.valueOf(valori[length-1])/*.substring(0,3)*/;



        long newRowId = DbUtils.insertDataInDb(fruitSelected, composeLocationString(location, "/"), sSpectrum);

        //Toast.makeText(Fruitipy.getAppContext(), "New insertion in DB: " + String.valueOf(newRowId), Toast.LENGTH_SHORT).show();

        startActivityForResult(generateDetailActivityIntent(newRowId, String.valueOf(fruitSelected)), REQUEST_NEW_SAMPLE);
    }

    private String composeLocationString(Location location, String delimiter){
        //Get last location
        String loc_string = "";
        try {
            loc_string += String.valueOf(new DecimalFormat("#.####").format(location.getLatitude())).replace(",",".") + delimiter + String.valueOf(new DecimalFormat("#.####").format(location.getLongitude())).replace(",",".");
        }
        catch (Exception e ){
            Toast.makeText(Fruitipy.getAppContext(), e.toString(), Toast.LENGTH_LONG).show();
            loc_string = "";
        }

        return loc_string;
    }

    public void showMessage(String msg) {
        /*
        Just to reduce the code
         */
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    /*
     * LISTENERS
     */

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        //Tab layout to change from sample view to photos view
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            switch(tab.getPosition()){
                case 0:
                    spectraRecyclerView.setVisibility(View.VISIBLE);
                    photosRecyclerView.setVisibility(View.GONE);
                    break;
                case 1:
                    spectraRecyclerView.setVisibility(View.GONE);
                    photosRecyclerView.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    View.OnClickListener menuButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!select_menu.isOpened()){
                select_layer.setVisibility(View.VISIBLE);
            }
            else{
                select_layer.setVisibility(View.GONE);
            }
            select_menu.toggle(true);
        }
    };
    View.OnClickListener selectLayerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(select_menu.isOpened()) {
                select_menu.toggle(true);
            }
            select_layer.setVisibility(View.GONE);
        }
    };

    /*
     * CALLBACKS
     */

    @Override
    public void onDeviceConnected(){
        changeButtonsColor(1);
        mState = UART_PROFILE_CONNECTED;
    }

    @Override
    public void onDeviceDisconnected(){
        changeButtonsColor(0);
        mState = UART_PROFILE_DISCONNECTED;
    }

    @Override
    public void onConnectionError(){
        progress.dismiss();
    }

    @Override
    public void onLocationUpdated(Location l, boolean m, boolean e){
        location = l;
        LocationHelper.onLocationUpdated(l,m,e,this,bottomBanner);
    }
}
