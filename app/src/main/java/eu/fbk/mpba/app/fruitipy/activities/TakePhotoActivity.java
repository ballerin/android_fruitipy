package eu.fbk.mpba.app.fruitipy.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.database.DbUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;

/**
 * Created by ballerin on 9/21/17.
 */

public class TakePhotoActivity extends AppCompatActivity {

    private String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private File photoFile;
    private String TAG = "TakePhotoAct";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        ButterKnife.bind(this);

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Log.e(TAG, "Error occurred creating the file");
                Log.e(TAG, ex.getLocalizedMessage());
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                Log.d(TAG, photoFile.getPath());

                Intent data = new Intent();
                data.setData(Uri.parse(photoFile.getPath()));
                setResult(RESULT_CANCELED, data);
            }
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fruit = "";
        String location = "";
        try {
            fruit = getIntent().getExtras().getString("FRUIT");
            location = getIntent().getExtras().getString("LOCATION");
        }
        catch(Exception e){
            fruit = "";
            location = "";
        }
        String imageFileName = "FRUITIPY_"+ fruit + "_" + location + "_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"Fruitipy");
        boolean success;
        if (!storageDir.exists()) {
            success = storageDir.mkdirs();
            Log.w(TAG, "Photos directory created: " + success);
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        //Toast.makeText(this, image.getAbsolutePath(), Toast.LENGTH_LONG).show();

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_TAKE_PHOTO:
                if(resultCode == -1){
                    DbUtils.insertPhotoRefInDb(photoFile.getPath());
                }
                break;
            default:
                break;
        }
        galleryAddPic();
        finish();
    }

    private void galleryAddPic() {
        Log.i(TAG,"LOOOOOOOOL");
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }
}
