package eu.fbk.mpba.app.fruitipy.network;

/**
 * Created by ballerin on 10/16/17.
 */

public class UploadObject {
    private String success;
    public UploadObject(String success) {
        this.success = success;
    }
    public String getSuccess() {
        return success;
    }
}