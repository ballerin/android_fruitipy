package eu.fbk.mpba.app.fruitipy.services;

import android.app.IntentService;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.HardwarePropertiesManager;
import android.util.Log;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.BuildConfig;
import eu.fbk.mpba.app.fruitipy.database.DatabaseContract;
import eu.fbk.mpba.app.fruitipy.database.DbHelper;
import eu.fbk.mpba.app.fruitipy.database.DbUtils;
import eu.fbk.mpba.app.fruitipy.network.NetworkUtils;
import eu.fbk.mpba.app.fruitipy.utils.Utils;
import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.models.ServerResponse;
import eu.fbk.mpba.app.fruitipy.models.Spectrum;
import eu.fbk.mpba.app.fruitipy.interfaces.RetrofitInterface;
import eu.fbk.mpba.app.fruitipy.activities.DetailActivity;
import eu.fbk.mpba.app.fruitipy.network.UploadObject;
import eu.fbk.mpba.app.fruitipy.utils.TimeAndDateUtils;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static eu.fbk.mpba.app.fruitipy.models.SpectrumHelper.getSpectrumById;

/**
 * Created by ballerin on 20/1/17.
 */

public class SyncService extends IntentService {
    /*
    This Service takes care of checking if new requests are present in the db. If there are new samples they are synced
     */

    private static int START_DELAY = 1;
    private static int UPDATE_DELAY = 1; //how much to wait between sync checks in seconds
    private static int PHOTO_DELAY = 1;

    private DbHelper mDbHelper;
    private SQLiteDatabase db;
    private RetrofitInterface service;
    private Set<Long> enqueued;
    private Set<String> imagesEnqueued;
    public static Service se;
    public static String token;
    private boolean stopEnqueuing;

    String TAG = "SyncService";

    public SyncService() {
        super("SyncService");
        mDbHelper = new DbHelper(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        TimeAndDateUtils.waitInSeconds(START_DELAY);
        se = this;
        db = mDbHelper.getWritableDatabase();

        enqueued = new HashSet<Long>();
        imagesEnqueued = new HashSet<String>();
        Log.d(TAG, "Started");
        int i = 0;

        //Quasi-infinite loop
        do{
            token = getSharedPreferences("saved_token", MODE_PRIVATE).getString("token", null);

            //Log.d(TAG, "Check number: " + i);
            if(areThereNewValues()) {
                syncDatabases();
            }

            if(areThereNewPhotos()) {
                syncPhotos();
            }

            TimeAndDateUtils.waitInSeconds(UPDATE_DELAY);
            i++;
        }
        while (i >= 0 && token != null);
    }

    /**
     * Checks if there are new spectra to sync (push to the remote server)
     * This function does not retrieve changes from the server
     * @return true if there are new samples to update, false otherwise
     */
    private boolean areThereNewValues() {
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                ,
        };

        String selection = DatabaseContract.Strings.COLUMN_NAME_TO_UPDATE + " = ?";


        String[] selectionArgs = {"1"};

        Cursor countRow = db.query(
                DatabaseContract.Strings.TABLE_NAME,      // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // The sort order
        );
        int count = countRow.getCount();
        countRow.close();
        //Log.d(TAG, "There are " + String.valueOf(count) + " rows to sync");
        return (count > 0);
    }

    /**
     * Checks if there are new photos to sync (push to the remote server)
     * This function does not retrieve changes from the server
     * @return true if there are new photos to upload, false otherwise
     */
    private boolean areThereNewPhotos(){
        //Log.d(TAG, String.valueOf(DbUtils.howManyPhotosToSyncInDb()));
        return DbUtils.howManyPhotosToSyncInDb()>0;
    }

    /**
     * Get the spectra queue to upload
     * @return
     */
    private Queue<Long> retrieveQueueFromDb(){
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DatabaseContract.Strings._ID,
        };

        String selection = DatabaseContract.Strings.COLUMN_NAME_TO_UPDATE + " = ?";

        String[] selectionArgs = {Integer.valueOf(1).toString()};

        Cursor cursor = db.query(
                DatabaseContract.Strings.TABLE_NAME,      // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // The sort order
        );
        Queue<Long> queue = Utils.getQueue();
        do{
            cursor.moveToNext();
            if(!cursor.isAfterLast()) {
                long id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.Strings._ID));
                if(!enqueued.contains(id)) {
                    queue.add(id);
                }
            }
        }while(!cursor.isLast() && !cursor.isAfterLast());
        cursor.close();
        return queue;
    }

    private void removeFromDbById(Long id){
        /*
        Function to remove the spectrum from the db when is synced with the server
         */

        // Define 'where' part of query.
        String selection = DatabaseContract.Strings._ID + " = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(id)};
        // Issue SQL statement.
        db.delete(DatabaseContract.Strings.TABLE_NAME, selection, selectionArgs);
    }

    /**
     * Main function
     *
     * It is checked if there are new samples to upload. If yes, they are uploaded.
     *
     */
    private void syncDatabases() {
        //Log.i(TAG, "Syncing...");

        Queue<Long> queue = retrieveQueueFromDb();

        long top;
        while(!queue.isEmpty()){
            top = queue.remove();
            Spectrum spectrumToSend = getSpectrumById(top);

            final long id = top;

            String formatted_time = TimeAndDateUtils.timestampToDjangoDate(spectrumToSend.getTime());

            service = NetworkUtils.getSpectrumRetrofit(token);

            Call<ServerResponse> call = service.insert(
                    spectrumToSend.spectrumAsString(),
                    spectrumToSend.getFruit(),
                    spectrumToSend.gpsAsString(),
                    formatted_time,
                    spectrumToSend.getCorrect(),
                    spectrumToSend.getNotes(),
                    spectrumToSend.getUni()
                    );

            enqueued.add(top);

            call.enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    //On response the id is removed from the queue TODO:find out if this can be made when the requests starts
                    enqueued.remove(id);
                    if(response.code()==201){
                        //if the record has been created successfully the record on the db is set as "uploaded"
                        ContentValues values = new ContentValues();
                        values.put(DatabaseContract.Strings.COLUMN_NAME_TO_UPDATE,0);

                        String selection = DatabaseContract.Strings._ID + " = ?";
                        String[] selectionArgs = {Long.valueOf(id).toString()};

                        String answer = response.body().getResult();
                        Log.d(TAG,"Success =>  " + answer);
                        values.put(DatabaseContract.Strings.COLUMN_NAME_RESULT, answer);
                        values.put(DatabaseContract.Strings.COLUMN_NAME_UNIVERSAL_ID, response.body().getUniId());

                        db.update(DatabaseContract.Strings.TABLE_NAME,values,selection,selectionArgs);

                        //update the result on the DetailActivity if it is shown
                        DetailActivity.newDataSynced(id);
                    }
                    else if(response.code() == 401){
                        //if the response is 401 (wrong credentials) then the token is outdated or wrong
                        //the user will be logged out
                        SharedPreferences.Editor editor = getSharedPreferences("saved_token", MODE_PRIVATE).edit();
                        editor.remove("token");
                        editor.commit();
                        call.cancel();
                        Toast.makeText(Fruitipy.getAppContext(), "401 - Login again", Toast.LENGTH_LONG).show();
                    }
                    else{
                        Log.d(TAG, "Status " + String.valueOf(response.code()));
                    }
                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    enqueued.remove(id);
                    Log.d(TAG, "Connection failed");
                }
            });
        }
    }


    /**
     * Main function
     *
     * It is checked if there are new photos to upload. If yes, they are uploaded.
     *
     */
    private void syncPhotos(){
        //TODO: write a retrievePhotosToUpdate function
        String[] projection = {
                DatabaseContract.Strings.COLUMN_NAME_PATH,
        };

        String selection = DatabaseContract.Strings.COLUMN_NAME_TO_UPDATE + " = ?";

        String[] selectionArgs = {"1"};

        Cursor cursor = db.query(
                DatabaseContract.Strings.PHOTOS_TABLE_NAME,      // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // The sort order
        );
        Queue<String> queue = new LinkedList<String>();
        do{
            cursor.moveToNext();
            if(!cursor.isAfterLast()) {
                String ref = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.Strings.COLUMN_NAME_PATH));
                if(!imagesEnqueued.contains(ref)) {
                    Log.d(TAG, "Added " + ref);
                    imagesEnqueued.add(ref);
                    queue.add(ref);
                }
            }
        }while(!cursor.isLast() && !cursor.isAfterLast());
        cursor.close();

        while(!queue.isEmpty()){
            String a = queue.remove();

            File file = new File(a);

            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

            Retrofit retrofit = NetworkUtils.getPhotoRetrofit(token);

            RetrofitInterface uploadImage = retrofit.create(RetrofitInterface.class);
            Call<UploadObject> fileUpload = uploadImage.uploadFile(fileToUpload, filename);

            //stopEnqueuing is necessary since a photo upload can take many seconds.
            //this is made to avoid uploading the same photo multiple times
            if(!stopEnqueuing) {
                stopEnqueuing = true;
                fileUpload.enqueue(new Callback<UploadObject>() {
                    @Override
                    public void onResponse(Call<UploadObject> call, Response<UploadObject> response) {
                        imagesEnqueued.remove(a);
                        stopEnqueuing = false;
                        if (response.code() == 201) {
                            //DbUtils.insertPhotoRefInDb(a);
                            String selection = DatabaseContract.Strings.COLUMN_NAME_PATH + " = ?";
                            String[] selectionArgs = {a};
                            ContentValues values = new ContentValues();
                            values.put(DatabaseContract.Strings.COLUMN_NAME_TO_UPDATE, "0");

                            db.update(DatabaseContract.Strings.PHOTOS_TABLE_NAME, values, selection, selectionArgs);
                        }
                        else{
                            Log.d(TAG, "Error uploading image: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<UploadObject> call, Throwable t) {
                        Log.d(TAG, "Error " + t.getMessage());
                        imagesEnqueued.remove(a);
                        stopEnqueuing = false;
                    }
                });
            }
            TimeAndDateUtils.waitInSeconds(PHOTO_DELAY);
        }
    }
}