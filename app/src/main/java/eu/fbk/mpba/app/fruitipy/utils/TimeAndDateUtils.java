package eu.fbk.mpba.app.fruitipy.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TimeAndDateUtils {
    public static String timestampToDjangoDate(long time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ITALY);
        sdf.setTimeZone(Calendar.getInstance().getTimeZone());
        return sdf.format(time* 1000);
    }

    /**
     * Just a delay function in seconds
     * @param seconds
     */
    public static void waitInSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
