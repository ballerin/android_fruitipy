package eu.fbk.mpba.app.fruitipy.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import eu.fbk.mpba.app.fruitipy.Fruitipy;
import eu.fbk.mpba.app.fruitipy.models.Token;
import eu.fbk.mpba.app.fruitipy.interfaces.RetrofitInterface;
import eu.fbk.mpba.app.fruitipy.network.TokenManager;
import eu.fbk.mpba.app.fruitipy.R;
import eu.fbk.mpba.app.fruitipy.services.SyncService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ballerin on 11/23/16.
 */

public class LoginActivity extends AppCompatActivity {
    /*
    TODO: add a progressDialog to avoid multiple token requests
     */
    @BindView(R.id.usernameText) TextView textUser;
    @BindView(R.id.passwordText) TextView textPasswd;

    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences prefs = getSharedPreferences("saved_token", MODE_PRIVATE);
        String token = prefs.getString("token", null);
        i = new Intent(this, MainActivity.class);
        if (token == null) {
            Toast.makeText(this, getString(R.string.no_token_saved), Toast.LENGTH_LONG).show();
        }
        else {
            Intent msgIntent = new Intent(Fruitipy.getAppContext(), SyncService.class);
            startService(msgIntent);
            startActivity(i);
            finish();
        }
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                recreate();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.AuthButton)
    public void auth(Button button) {
        String user = textUser.getText().toString();
        String passwd = textPasswd.getText().toString();

        RetrofitInterface service = TokenManager.getTokenManager(user, passwd);
        Call<Token> call = service.login();

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if(!isResponseValid(response)){
                    Toast.makeText(Fruitipy.getAppContext(), getString(R.string.something_whent_wrong_server), Toast.LENGTH_SHORT).show();
                    return;
                }
                String text_response = response.body().getToken();
                if(text_response == null || text_response.equals("invalid")){ //Check if token received is valid or not
                    Toast.makeText(Fruitipy.getAppContext(), getString(R.string.token_received_is_invalid),Toast.LENGTH_SHORT).show();
                }
                else {
                    SharedPreferences.Editor editor = getSharedPreferences("saved_token", MODE_PRIVATE).edit();
                    editor.putString("token", text_response);
                    editor.commit();
                    //Toast.makeText(Fruitipy.getAppContext(), "Token set as " + text_response, Toast.LENGTH_SHORT).show();
                    Intent msgIntent = new Intent(Fruitipy.getAppContext(), SyncService.class);
                    startService(msgIntent);
                    startActivity(i);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Toast.makeText(Fruitipy.getAppContext(), getString(R.string.failure_server), Toast.LENGTH_SHORT).show();
                Log.e("failure",t.toString());

            }
        });

    }

    public static boolean isResponseValid(Response<Token> response){
        if(response.code()!=200)
            return false;
        try {
            response.body().getToken();
        }
        catch(Exception e){
            return false;
        }
        return true;
    }
}
